$('document').ready(function () {
    let btnDeleted = $('#btnDelted');
    if (btnDeleted) {
        btnDeleted.click(function () {
            swal({
                title: 'Êtes-vous sure?',
                text: "Voulez-vous supprimez définitivement cette donnée!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oui, Supprimez le!'
            }).then(result => {
                console.info(result);
                if (result.value) {
                    let uri = '/category/delete/' + $('#idCategory').val();
                    console.info("Uri " + uri);
                    $.ajax({
                        url: uri,
                        type: 'DELETE',
                        success: function (result) {
                            console.info(result);
                            if (result) {
                                swal({
                                    title: 'La donnée vient d\'être supprimé',
                                    text: "Cliquez sur le bouton pour recharger la page",
                                    type: 'success',
                                    allowOutsideClick: false
                                }).then(result => {
                                    console.info(result);
                                    location.assign("/categories");
                                });
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.error(errorThrown);
                            swal({
                                title: "Erreur",
                                text: "Assurez-vous que la donnée n'est pas utilisé dans une autre de vos données",
                                type: 'error'
                            });
                        }
                    });
                }
            })
        });
    }
});