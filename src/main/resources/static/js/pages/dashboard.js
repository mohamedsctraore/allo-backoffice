let satisfactionOpinionData = [0,0];

function initializeChart() {
    let ctx = $('#opinionChart');

    let opinionChart = new Chart(ctx, {
        type: 'pie',
        data: {
            datasets: [{
                data: satisfactionOpinionData,
                backgroundColor: [
                    'rgba(78,229,8,1)',
                    'rgba(253,12,70,1)'
                ],
                hoverBorderColor: [
                    'rgba(78,229,8,1)',
                    'rgba(253,12,70,1)'
                ]
            }],
            labels: [
                'Satisfait',
                'Mécontant'
            ]
        },
        options: {
            responsive: true
        }
    });
}

function getSatisfactionData() {
    axios.get('/opinions/satisfaction/average/chart')
        .then((response) => {
            console.info('Status info : ' + response.status);
            let data = response.data;
            console.info('Data info : ' + data);
            console.info(satisfactionOpinionData);
            if (data) {
                satisfactionOpinionData = [];
                satisfactionOpinionData.push(data.satisfied);
                satisfactionOpinionData.push(data.dissatisfied);
                console.info(satisfactionOpinionData);
                console.info('SatisfactionOpinionData info : ' + satisfactionOpinionData);
                initializeChart();
            }
        })
}


$('document').ready(function () {
    getSatisfactionData();
});