let passwordRepeated = $('#passwordRepeated');

let formPasswordRepeated = $('#formPasswordRepeated');

passwordRepeated.change(() => {
    console.info("Debut du Process 'onChange Password'");
    let password = $('#password').val();
    let passwordRepeated = $('#passwordRepeated').val();
    if (password === passwordRepeated) {
        $('#alertPassword').hide();
        $('#btnSubmit').prop('disabled', false);
    } else {
        $('#alertPassword').show();
        $('#btnSubmit').prop('disabled', true);
    }
});

formPasswordRepeated.change(() => {
    let password = $('#formPassword').val();
    let passwordRepeated = $('#formPasswordRepeated').val();
    if (password === passwordRepeated) {
        $('#formAlertPassword').hide();
        $('#formBtnSubmit').prop('disabled', false);
    } else {
        $('#formAlertPassword').show();
        $('#formBtnSubmit').prop('disabled', true);
    }
});

formPasswordRepeated.click(() => {
    let password = $('#formPassword').val();
    let passwordRepeated = $('#formPasswordRepeated').val();
    if (password === passwordRepeated) {
        $('#formAlertPassword').hide();
        $('#formBtnSubmit').prop('disabled', false);
    } else {
        $('#formAlertPassword').show();
        $('#formBtnSubmit').prop('disabled', true);
    }
});


passwordRepeated.click(() => {
    let password = $('#password').val();
    let passwordRepeated = $('#passwordRepeated').val();
    if (password === passwordRepeated) {
        $('#alertPassword').hide();
        $('#btnSubmit').prop('disabled', false);
    } else {
        $('#alertPassword').show();
        $('#btnSubmit').prop('disabled', true);
    }
});

let privileges = [
    {
        label: 'Bronze',
        name: 'ADMIN_SMALL'
    }, {
        label: 'Argent',
        name: 'ADMIN_MEDIUM'
    }, {
        label: 'Or',
        name: 'ADMIN_BIG'
    }
];

let MyDateField = function (config) {
    jsGrid.Field.call(this, config);
};

function configDatePickerUi() {
    $.datepicker.regional['fr'] = {
        closeText: 'Fermer',
        prevText: '&#x3c;Préc',
        nextText: 'Suiv&#x3e;',
        currentText: 'Aujourd\'hui',
        monthNames: ['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin',
            'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Decembre'],
        monthNamesShort: ['Jan', 'Fev', 'Mar', 'Avr', 'Mai', 'Jun',
            'Jul', 'Aou', 'Sep', 'Oct', 'Nov', 'Dec'],
        dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
        dayNamesShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
        dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
        weekHeader: 'Sm',
        dateFormat: 'dd-mm-yy',
        showButtonPanel: true
    };
    $.datepicker.setDefaults($.datepicker.regional['fr']);
}

MyDateField.prototype = new jsGrid.Field({
    css: "date-field",
    align: "center",
    sorter: function (date1, date2) {
        return new Date(date1) - new Date(date2);
    },
    itemTemplate: function (value) {
        return new Date(value).toLocaleDateString();
    },
    insertTemplate: function (value) {
        return this._insertPicker = $("<input>").datepicker({defaultDate: new Date()});
    },
    editTemplate: function (value) {
        return this._editPicker = $("<input>").datepicker().datepicker("setDate", new Date(value));
    },
    insertValue: function () {
        return this._insertPicker.datepicker("getDate").toISOString();
    },
    editValue: function () {
        return this._editPicker.datepicker("getDate").toISOString();
    }
});

jsGrid.fields.date = MyDateField;

function initGrid() {
    $('#tabAdministrators').jsGrid({
        height: "auto",
        width: "100%",

        editing: true,
        sorting: true,
        paging: true,
        autoload: true,

        onInit: (grid) => {
            console.log('start onInit.');
        },

        onError: (grid, args) => {
            console.log('... start onError.');
            console.log(grid);
            console.log(args);
            console.log('... end onError.');
        },

        deleteConfirm: "Voulez-vous vraiment supprimer l'administrateur ?",

        onDataLoaded: (d) => {
            console.log('... start onDataLoaded');
        },
        onItemUpdated: (args) => {
            console.info("Debut du Process 'onItemUpdated :: JSGrid'");
            let administrator = args.item;
            console.info(administrator);
            updateAdministrator(administrator);
        },
        onItemDeleted: (args) => {
            console.info("Debut du Process 'onItemDeleted :: JSGrid'");
            let administrator = args.item;
            console.info(administrator);
            let id = administrator.user.id;
            deleteAdministrator(id);
        },
        controller: {
            loadData: () => {
                return loadChildrenAdmin();
            }
        },
        fields: [
            {
                title: "Identifiant",
                name: "id",
                align: "center",
                type: "text",
                validate: "required",
                width: 50,
                editing: false
            },
            {
                title: "Prenom",
                name: "user.firstName",
                align: "center",
                type: "text",
                validate: "required",
                width: 80
            }, {
                title: "Nom",
                name: "user.lastName",
                align: "center",
                type: "text",
                validate: "required",
                width: 80
            },
            {
                title: "Login",
                name: "user.login",
                align: "center",
                type: "text",
                validate: "required",
                width: 80
            }, {
                title: "Email",
                name: "user.email",
                align: "center",
                type: "text",
                validate: "required",
                width: 80
            }, {
                title: "Contact Mobile",
                name: "user.phoneNumber",
                align: "center",
                type: "text",
                validate: "required",
                width: 80
            }, {
                title: "Date de Naissance",
                name: "user.birthDate",
                align: "center",
                type: "date",
                validate: "required",
                width: 80
            }, {
                title: "Crée le",
                name: "user.created",
                align: "center",
                type: "date",
                validate: "required",
                width: 80,
                editing: false
            }, {
                title: "Compte Activée",
                name: "user.enabled",
                align: "center",
                type: "checkbox",
                validate: "required",
                width: 80
            }, {
                title: "Privilège",
                name: "user.privilege",
                align: "center",
                type: "select",
                validate: "required",
                items: privileges,
                width: 80,
                valueField: "name",
                textField: "label"
            }, {
                type: "control",
                editButton: true,
                deleteButton: true,
                align: "center",
                width: 50
            }
        ]
    });
}

function loadChildrenAdmin() {
    console.info("Debut du Process 'loadChildrenAdmin'");
    let deferred = $.Deferred();
    axios.get('/administrators/children')
        .then((response) => {
            console.info(response);
            let status = response.status;
            if (status === 200) {
                console.info(response.data);
                deferred.resolve(response.data);
            }
        }).catch((error) => {
        console.info("La récupération des données a échoués");
        console.error(error);
    });
    return deferred.promise();
}

function updateAdministrator(administrator) {
    console.info("Debut du Process 'updateAdministrator'");
    axios.put('/administrators/children', administrator)
        .then((response) => {
            let status = response.status;
            if (status === 200) {
                let data = response.data;
                console.info(data);
                if (data)
                    $('#tabAdministrators').loadData();
                else
                    console.info("La mise à échoué");
            }
        }).catch((error) => {
        console.info("La mise à jour de la données a échoué");
        console.error(error);
    });
}

function deleteAdministrator(id) {
    console.info("Debut du Process 'deleteAdministrator'");
    axios.delete('/administrators/deleted/'+id)
        .then((response) => {
            let status = response.status;
            if (status === 200) {
                let data = response.data;
                console.info(data);
                if (data)
                    $('#tabAdministrators').loadData();
                else
                    console.info("La suppression a échoué");
            }
        }).catch((error) => {
        console.info("La suppression a échoué");
        console.error(error);
    });
}

$('document').ready(() => {
    $('#alertPassword').hide();
    initGrid();
    configDatePickerUi();
});