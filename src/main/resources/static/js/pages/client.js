let passwordRepeated = $('#passwordRepeated');

passwordRepeated.change(() => {
    let password = $('#password').val();
    let passwordRepeated = $('#passwordRepeated').val();
    if (password === passwordRepeated) {
        $('#alertPassword').hide();
        $('#btnSubmit').prop('disabled', false);
    } else {
        $('#alertPassword').show();
        $('#btnSubmit').prop('disabled', true);
    }
});

passwordRepeated.click(() => {
    let password = $('#password').val();
    let passwordRepeated = $('#passwordRepeated').val();
    if (password === passwordRepeated) {
        $('#alertPassword').hide();
        $('#btnSubmit').prop('disabled', false);
    } else {
        $('#alertPassword').show();
        $('#btnSubmit').prop('disabled', true);
    }
});

$('document').ready(() => {
    $('#alertPassword').hide();
    let btnDeleted = $('#btnDeleted');
    console.info(btnDeleted);
    if (btnDeleted) {
        btnDeleted.click(function () {
            swal({
                title: 'Êtes-vous sure?',
                text: "Voulez-vous supprimez définitivement cette donnée!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oui, Supprimez le!'
            }).then(result => {
                console.info(result);
                if (result.value) {
                    let uri = '/customer/delete/' + $('#idCustomer').val();
                    console.info("Uri " + uri);
                    $.ajax({
                        url: uri,
                        type: 'DELETE',
                        success: function (result) {
                            console.info(result);
                            if (result) {
                                swal({
                                    title: 'La donnée vient d\'être supprimé',
                                    text: "Cliquez sur le bouton pour recharger la page",
                                    type: 'success',
                                    allowOutsideClick: false
                                }).then(result => {
                                    console.info(result);
                                    location.assign("/artisans");
                                });
                            }
                        }
                    });
                }
            })
        });
    }
});