let passwordRepeated = $('#passwordRepeated');

passwordRepeated.change(() => {
    let password = $('#password').val();
    let passwordRepeated = $('#passwordRepeated').val();
    if(password === passwordRepeated) {
        $('#alertPassword').hide();
    } else {
        $('#alertPassword').show();
    }
});

passwordRepeated.click(() => {
    let password = $('#password').val();
    let passwordRepeated = $('#passwordRepeated').val();
    if(password === passwordRepeated) {
        $('#alertPassword').hide();
    } else {
        $('#alertPassword').show();
    }
});

$('document').ready(() => {
    $('#alertPassword').hide();
});