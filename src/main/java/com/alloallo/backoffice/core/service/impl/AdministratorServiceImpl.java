package com.alloallo.backoffice.core.service.impl;

import com.alloallo.backoffice.core.entity.Administrator;
import com.alloallo.backoffice.core.entity.Users;
import com.alloallo.backoffice.core.repository.AdministratorRepository;
import com.alloallo.backoffice.core.service.AdministratorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

/**
 * @author Christian Amani on 28/10/2018.
 */
@Service
public class AdministratorServiceImpl implements AdministratorService {

    private final Logger LOG = LoggerFactory.getLogger(AdministratorServiceImpl.class);
    private final AdministratorRepository repository;

    @Autowired
    public AdministratorServiceImpl(AdministratorRepository repository) {
        this.repository = repository;
    }

    @Override
    public Administrator save(Administrator administrator) {
        LOG.info("Debut du Process 'save'");
        if(administrator != null)
            return repository.save(administrator);
        return null;
    }

    @Override
    public Administrator find(Users user) {
        LOG.info("Debut du Process 'find'");
        if(user != null)
            return repository.findByUser(user);
        return null;
    }

    @Override
    public List<Administrator> find(Administrator parent) {
        LOG.info("Debut du Process 'find'");
        if(parent != null) {
            Users user = parent.getUser();
            if(user != null)
                return repository.findByUser_Parent(user);
        }
        return Collections.emptyList();
    }

    @Override
    public long count(Administrator parent) {
        LOG.info("Debut du Process 'count'");
        if(parent != null) {
            Users user = parent.getUser();
            if(user != null)
                return repository.countByUser_Parent(user);
        }
        return 0;
    }
}
