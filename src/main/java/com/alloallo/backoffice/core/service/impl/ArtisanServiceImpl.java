package com.alloallo.backoffice.core.service.impl;

import com.alloallo.backoffice.core.entity.Administrator;
import com.alloallo.backoffice.core.entity.Artisan;
import com.alloallo.backoffice.core.entity.Category;
import com.alloallo.backoffice.core.entity.Users;
import com.alloallo.backoffice.core.model.Profile;
import com.alloallo.backoffice.core.repository.ArtisanRepository;
import com.alloallo.backoffice.core.service.ArtisanService;
import com.alloallo.backoffice.core.service.CategoryService;
import com.alloallo.backoffice.core.service.UsersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author Christian Amani on 01/11/2018.
 */
@Service
public class ArtisanServiceImpl implements ArtisanService {

    private final Logger LOG = LoggerFactory.getLogger(ArtisanServiceImpl.class);
    private final ArtisanRepository repository;
    private final CategoryService categoryService;
    private final HttpSession httpSession;
    private final UsersService usersService;


    @Autowired
    public ArtisanServiceImpl(ArtisanRepository repository, CategoryService categoryService
            , HttpSession httpSession, UsersService usersService) {
        this.repository = repository;
        this.categoryService = categoryService;
        this.httpSession = httpSession;
        this.usersService = usersService;
    }


    @Override
    public Artisan save(Artisan artisan, boolean isCreated) {
        LOG.info("Debut du Process 'save'");
        if (artisan != null) {
            Users user;
            if (isCreated)
                user = usersService.save(artisan);
            else {
                user = artisan.getUser();
                user = usersService.save(user, true, artisan);
            }
            if (user == null)
                return null;
            return artisan;
        }
        return null;
    }

    @Override
    public Artisan find(long id) {
        LOG.info("Debut du Process 'find'");
        return repository.findById(id).orElse(null);
    }

    @Override
    public List<Artisan> find() {
        LOG.info("Debut du Process 'find'");
        return repository.findAll();
    }

    @Override
    public long count(Administrator administrator) {
        LOG.info("Debut du Process 'count'");
        if (administrator != null)
            return repository.countByAdministrator(administrator);
        return 0;
    }

    @Override
    public boolean delete(long id) {
        LOG.info("Debut du Process 'delete'");
        LOG.info("Address IP qui Execute le Process " + httpSession.getAttribute("IP"));
        Artisan artisan = find(id);
        if (artisan != null) {
            Category category = artisan.getCategory();
            if (category != null) {
                category.removeArtisan(artisan);
                categoryService.save(category);
            }
            Users user = artisan.getUser();
            if (user != null) {
                long userId = user.getId();
                return usersService.delete(userId, Profile.Role.ARTISAN);
            }
        }
        return false;
    }
}
