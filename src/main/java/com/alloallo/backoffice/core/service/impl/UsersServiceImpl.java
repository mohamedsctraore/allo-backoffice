package com.alloallo.backoffice.core.service.impl;

import com.alloallo.backoffice.core.entity.*;
import com.alloallo.backoffice.core.model.Profile;
import com.alloallo.backoffice.core.repository.AdministratorRepository;
import com.alloallo.backoffice.core.repository.ArtisanRepository;
import com.alloallo.backoffice.core.repository.CustomerRepository;
import com.alloallo.backoffice.core.repository.UserRepository;
import com.alloallo.backoffice.core.service.CategoryService;
import com.alloallo.backoffice.core.service.UsersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;
import java.time.Clock;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * @author Christian Amani on 21/10/2018.
 */
@Service
public class UsersServiceImpl implements UsersService {

    private Logger LOG = LoggerFactory.getLogger(UsersServiceImpl.class);

    private final PasswordEncoder passwordEncoder;
    private final UserRepository repository;
    private final ArtisanRepository artisanRepository;
    private final CustomerRepository customerRepository;
    private final CategoryService categoryService;
    private final AdministratorRepository administratorRepository;
    private final HttpSession httpSession;


    public UsersServiceImpl(PasswordEncoder passwordEncoder, UserRepository repository
            , ArtisanRepository artisanRepository, CustomerRepository customerRepository
            , CategoryService categoryService, AdministratorRepository administratorRepository, HttpSession httpSession) {
        this.passwordEncoder = passwordEncoder;
        this.repository = repository;
        this.artisanRepository = artisanRepository;
        this.customerRepository = customerRepository;
        this.categoryService = categoryService;
        this.administratorRepository = administratorRepository;
        this.httpSession = httpSession;
    }

    @Override
    public Users save(Users user, boolean isCreated) {
        LOG.info("Debut du Process 'save'");
        if (user != null && !verifyLoginIfExist(user.getLogin())) {
            encodeUserPassword(user);
            user.setCreated(LocalDate.now(Clock.systemUTC()));
            Users.Privilege privilege = user.getPrivilege();
            user = repository.save(user);
            user.setEnabled(true);
            if (!isCreated)
                return user;
            switch (privilege) {
                case ARTISAN:
                    Artisan artisan = new Artisan();
                    artisan.setUser(user);
                    artisanRepository.save(artisan);
                    return user;
                case CUSTOMER:
                    Customer customer = new Customer();
                    customer.setUser(user);
                    customerRepository.save(customer);
                    return user;
                case ADMIN_BIG:
                    saveAdministrator(user);
                    return user;
                case ADMIN_SMALL:
                    saveAdministrator(user);
                    return user;
                case ADMIN_MEDIUM:
                    saveAdministrator(user);
                    return user;
            }
        }
        return null;
    }

    @Override
    public Users save(Users user, boolean isUpdatedPassword, Artisan artisan) {
        LOG.info("Debut du Process 'save'");
        if (user != null && verifyLoginIfExist(user.getLogin())) {
            if (isUpdatedPassword) {
                user.setCreated(LocalDate.now(Clock.systemUTC()));
                encodeUserPassword(user);
            }
            user = repository.save(user);
            artisan.setUser(user);
            artisanRepository.save(artisan);
            return user;
        }
        return null;
    }

    @Override
    public Users save(Artisan artisan) {
        LOG.info("Debut du Process 'save'");
        if (artisan != null) {
            Users user = artisan.getUser();
            if (user != null && !verifyLoginIfExist(user.getLogin())) {
                user.setCreated(LocalDate.now(Clock.systemUTC()));
                encodeUserPassword(user);
                user.setRole(Profile.Role.ARTISAN);
                user.setPrivilege(Users.Privilege.ARTISAN);
                user = repository.save(user);
                artisan.setUser(user);
                Category category = artisan.getCategory();
                category = categoryService.find(category.getId());
                category.addArtisan(artisan);
                artisanRepository.save(artisan);
                return user;
            }
        }
        return null;
    }

    @Override
    public Users save(Users user, boolean isUpdatedPassword, Administrator administrator) {
        LOG.info("Debut du Process 'save'");
        if (user != null && verifyLoginIfExist(user.getLogin()) && administrator != null) {
            if (isUpdatedPassword) {
                user.setCreated(LocalDate.now(Clock.systemUTC()));
                encodeUserPassword(user);
            }
            user = repository.save(user);
            administrator.setUser(user);
            administratorRepository.save(administrator);
            return user;
        }
        return null;
    }

    @Override
    public Users save(Administrator administrator) {
        LOG.info("Debut du Process 'administrator'");
        if (administrator != null) {
            Users user = administrator.getUser();
            if (user != null && !verifyLoginIfExist(user.getLogin())) {
                user.setCreated(LocalDate.now(Clock.systemUTC()));
                encodeUserPassword(user);
                user.setRole(Profile.Role.ADMIN);
                user = repository.save(user);
                administrator.setUser(user);
                administratorRepository.save(administrator);
                return  user;
            }
        }
        return null;
    }

    @Override
    public Users save(Users user, boolean isUpdatedPassword, Customer customer) {
        LOG.info("Debut du Process 'save'");
        if (user != null && verifyLoginIfExist(user.getLogin()) && customer != null) {
            if (isUpdatedPassword) {
                user.setCreated(LocalDate.now(Clock.systemUTC()));
                encodeUserPassword(user);
            }
            user = repository.save(user);
            customer.setUser(user);
            customerRepository.save(customer);
            return user;
        }
        return null;
    }

    @Override
    public Users save(Customer customer) {
        LOG.info("Debut du Process 'save'");
        if (customer != null) {
            Users user = customer.getUser();
            if (user != null && !verifyLoginIfExist(user.getLogin())) {
                user.setCreated(LocalDate.now(Clock.systemUTC()));
                encodeUserPassword(user);
                user.setRole(Profile.Role.CUSTOMER);
                user.setPrivilege(Users.Privilege.CUSTOMER);
                user = repository.save(user);
                customer.setUser(user);
                customerRepository.save(customer);
                return user;
            }
        }
        return null;
    }

    private void saveAdministrator(Users user) {
        Administrator administrator = new Administrator();
        administrator.setUser(user);
        administratorRepository.save(administrator);
    }

    @Override
    public Users find(long id) {
        LOG.info("Debut du Process 'find'");
        return repository.findById(id).orElse(null);
    }

    @Override
    public List<Users> find() {
        LOG.info("Debut du Process 'find'");
        return repository.findAll();
    }

    @Override
    public String findPassword(long id) {
        LOG.info("Debut du Process 'findPassword'");
        Users user = find(id);
        if (user != null)
            return user.getPassword();
        return null;
    }

    @Override
    public boolean delete(long id) {
        LOG.info("Debut du Process 'delete'");
        LOG.info("Address IP qui Execute le Process " + httpSession.getAttribute("IP"));
        Optional<Users> optionalUser = repository.findById(id);
        if(optionalUser.isPresent()) {
            Users user = optionalUser.get();
            Profile.Role role = user.getRole();
            if(role != null)
                return delete(id,role);
            repository.deleteById(id);
            Optional<Users> userWhichRemoved = repository.findById(id);
            return !userWhichRemoved.isPresent();
        }
        return false;
    }

    @Transactional
    @Override
    public boolean delete(long id, Profile.Role role) {
        LOG.info("Debut du Process 'delete'");
        LOG.info("Address IP qui Execute le Process " + httpSession.getAttribute("IP"));
        switch (role) {
            case CUSTOMER: {
                customerRepository.deleteByUser_Id(id);
                LOG.debug("CUSTOMER DELETED");
                break;
            }
            case ARTISAN: {
                artisanRepository.deleteByUser_Id(id);
                LOG.debug("ARTISAN DELETED");
                break;
            }
            case ADMIN: {
                administratorRepository.deleteByUser_Id(id);
                LOG.debug("ADMINISTRATOR DELETED");
                break;
            }
        }
        repository.deleteById(id);
        Optional<Users> userWhichRemoved = repository.findById(id);
        return !userWhichRemoved.isPresent();
    }

    @Override
    public boolean verifyPassword(long id, String oldPassword) {
        LOG.info("Debut du Process 'verifyPassword'");
        LOG.info("Address IP qui Execute le Process " + httpSession.getAttribute("IP"));
        Optional<Users> optionalUser = repository.findById(id);
        if (optionalUser.isPresent()) {
            Users user = optionalUser.get();
            String password = user.getPassword();
            return passwordEncoder.matches(oldPassword, password);
        }
        return false;
    }

    @Override
    public boolean verifyLoginIfExist(String login) {
        return repository.existsByLogin(login);
    }

    private void encodeUserPassword(Users user) {
        String password = user.getPassword();
        password = passwordEncoder.encode(password);
        user.setPassword(password);
    }
}
