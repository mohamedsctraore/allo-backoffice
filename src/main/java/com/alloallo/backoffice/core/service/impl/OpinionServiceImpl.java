package com.alloallo.backoffice.core.service.impl;

import com.alloallo.backoffice.core.entity.Opinion;
import com.alloallo.backoffice.core.repository.OpinionRepository;
import com.alloallo.backoffice.core.service.OpinionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Christian Amani on 06/11/2018.
 */
@Service
public class OpinionServiceImpl implements OpinionService {

    private final Logger LOG = LoggerFactory.getLogger(OpinionServiceImpl.class);
    private final OpinionRepository repository;

    public OpinionServiceImpl(OpinionRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Opinion> find() {
        LOG.info("Debut du Process 'find'");
        return repository.findAll();
    }

    @Override
    public double findAverageDissatisfied() {
        LOG.info("Debut du Process 'findAverageDissatisfied'");
        List<Opinion> opinions = repository.findAll();
        return opinions.stream()
                .filter(opinion -> opinion.getStars() < 3)
                .mapToLong(Opinion::getStars)
                .average().orElse(0);
    }

    @Override
    public double findAverageSatisfied() {
        LOG.info("Debut du Process 'findAverageSatisfied'");
        List<Opinion> opinions = repository.findAll();
        return opinions.stream()
                .filter(opinion -> opinion.getStars() >= 3)
                .mapToLong(Opinion::getStars)
                .average().orElse(0);
    }
}
