package com.alloallo.backoffice.core.service.impl;

import com.alloallo.backoffice.core.entity.Opinion;
import com.alloallo.backoffice.core.repository.ArtisanRepository;
import com.alloallo.backoffice.core.repository.CustomerRepository;
import com.alloallo.backoffice.core.repository.OpinionRepository;
import com.alloallo.backoffice.core.service.HomeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author  Christian Amani on 22/10/2018.
 */
@Service
public class HomeServiceImpl implements HomeService {

    private final Logger LOG = LoggerFactory.getLogger(HomeServiceImpl.class);
    private final ArtisanRepository artisanRepository;
    private final CustomerRepository customerRepository;
    private final OpinionRepository opinionRepository;

    public HomeServiceImpl(ArtisanRepository artisanRepository, CustomerRepository customerRepository
            , OpinionRepository opinionRepository) {
        this.artisanRepository = artisanRepository;
        this.customerRepository = customerRepository;
        this.opinionRepository = opinionRepository;
    }

    @Override
    public Map<String, Object> addInformationSummaryToModel() {
        LOG.info("Debut du Process 'addInformationSummaryToModel'");
        Map<String,Object> map = new HashMap<>();
        int nbrArtisans = artisanRepository.findAll().size();
        map.put("artisans",nbrArtisans);
        int nbrCustomers = customerRepository.findAll().size();
        map.put("customers",nbrCustomers);
        List<Opinion> opinions = opinionRepository.findAll();
        int nbrOpinions = opinions.size();
        map.put("opinions",nbrOpinions);
        map.put("opinionsList",opinions);
        return map;
    }
}
