package com.alloallo.backoffice.core.service;

import com.alloallo.backoffice.core.entity.Administrator;
import com.alloallo.backoffice.core.entity.Artisan;
import com.alloallo.backoffice.core.entity.Customer;
import com.alloallo.backoffice.core.entity.Users;
import com.alloallo.backoffice.core.model.Profile;

import java.util.List;

/**
 * @author Christian Amani on 21/10/2018.
 */
public interface UsersService {

    Users save(Users user, boolean isCreated);

    Users save(Users user, boolean isUpdatedPassword, Artisan artisan);

    Users save(Artisan artisan);

    Users save(Users user, boolean isUpdatedPassword, Administrator administrator);

    Users save(Administrator administrator);

    Users save(Users user, boolean isUpdatedPassword, Customer customer);

    Users save(Customer customer);

    Users find(long id);

    List<Users> find();

    String findPassword(long id);

    boolean delete(long id);

    boolean delete(long id, Profile.Role role);

    boolean verifyPassword(long id, String oldPassword);

    boolean verifyLoginIfExist(String login);
}
