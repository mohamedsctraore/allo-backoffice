package com.alloallo.backoffice.core.service;

import com.alloallo.backoffice.core.entity.Administrator;
import com.alloallo.backoffice.core.entity.Artisan;

import java.util.List;

/**
 * @author Christian Amani on 01/11/2018.
 */
public interface ArtisanService {

    Artisan save(Artisan artisan, boolean isCreated);

    Artisan find(long id);

    List<Artisan> find();

    long count(Administrator administrator);

    boolean delete(long id);
}
