package com.alloallo.backoffice.core.service;

import com.alloallo.backoffice.core.entity.Opinion;

import java.util.List;

/**
 * @author Christian Amani on 06/11/2018.
 */
public interface OpinionService {

    List<Opinion> find();

    double findAverageDissatisfied();

    double findAverageSatisfied();
}
