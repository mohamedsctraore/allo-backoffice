package com.alloallo.backoffice.core.service.impl;

import com.alloallo.backoffice.core.entity.Administrator;
import com.alloallo.backoffice.core.entity.Category;
import com.alloallo.backoffice.core.entity.Users;
import com.alloallo.backoffice.core.repository.CategoryRepository;
import com.alloallo.backoffice.core.service.AdministratorService;
import com.alloallo.backoffice.core.service.CategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

/**
 * @author Christian Amani on 23/10/2018.
 */
@Service
public class CategoryServiceImpl implements CategoryService {

    private final Logger LOG = LoggerFactory.getLogger(CategoryServiceImpl.class);
    private final CategoryRepository repository;
    private final AdministratorService administratorService;
    private final HttpSession httpSession;

    @Autowired
    public CategoryServiceImpl(CategoryRepository repository, AdministratorService administratorService, HttpSession httpSession) {
        this.repository = repository;
        this.administratorService = administratorService;
        this.httpSession = httpSession;
    }

    @Override
    public Category save(Category category) {
        LOG.info("Debut du Process 'save'");
        if (category != null) {
            category.setUpdated(LocalDate.now());
            Users user = (Users) httpSession.getAttribute("userObject");
            Administrator administrator = administratorService.find(user);
            if (administrator != null) {
                category.setAdministrator(administrator);
                category = repository.save(category);
                administrator.addCategory(category);
                administratorService.save(administrator);
                return category;
            }
        }
        return null;
    }

    @Override
    public Category find(long id) {
        LOG.info("Debut du Process 'find'");
        return repository.findById(id).orElse(null);
    }

    @Override
    public List<Category> find() {
        LOG.info("Debut du Process 'find'");
        return repository.findAll();
    }

    @Override
    public long count(Administrator administrator) {
        LOG.info("Debut du Process 'administrator'");
        if(administrator != null)
            return repository.countByAdministrator(administrator);
        return 0;
    }

    @Override
    public boolean delete(long id) {
        LOG.info("Debut du Process 'delete'");
        LOG.info("Address IP qui Execute le Process " + httpSession.getAttribute("IP"));
        Category category = find(id);
        Administrator administrator = category.getAdministrator();
        administrator.removeCategory(category);
        repository.deleteById(id);
        administratorService.save(administrator);
        return !repository.findById(id).isPresent();
    }

    @Override
    public void verifyDataDetail(Model model) {
        LOG.info("Debut du Process 'verifyDataDetail'");
        if (model != null) {
            final String CATEGORIES_ARTISAN = "categoriesArtisan";
            if (!model.containsAttribute(CATEGORIES_ARTISAN)) {
                model.addAttribute(CATEGORIES_ARTISAN, Collections.emptyList());
            }
            final String FORM_DETAIL_CATEGORY = "formDetailCategory";
            if (!model.containsAttribute(FORM_DETAIL_CATEGORY)) {
                model.addAttribute(new Category());
                model.addAttribute("showingDetail", false);
            }
        }
    }
}
