package com.alloallo.backoffice.core.service;

import com.alloallo.backoffice.core.entity.Customer;

import java.util.List;

/**
 * @author Christian Amani on 06/11/2018.
 */
public interface CustomerService {

    Customer save(Customer customer, boolean isCreated);

    List<Customer> find();

    Customer find(long id);

    boolean delete(long id);
}
