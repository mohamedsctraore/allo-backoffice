package com.alloallo.backoffice.core.service.impl;

import com.alloallo.backoffice.core.entity.Customer;
import com.alloallo.backoffice.core.entity.Users;
import com.alloallo.backoffice.core.model.Profile;
import com.alloallo.backoffice.core.repository.CustomerRepository;
import com.alloallo.backoffice.core.service.CustomerService;
import com.alloallo.backoffice.core.service.UsersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author Christian Amani on 06/11/2018.
 */
@Service
public class CustomerServiceImpl implements CustomerService {

    private final Logger LOG = LoggerFactory.getLogger(CustomerServiceImpl.class);
    private final HttpSession httpSession;
    private final CustomerRepository repository;
    private final UsersService usersService;

    @Autowired
    public CustomerServiceImpl(HttpSession httpSession, CustomerRepository repository, UsersService usersService) {
        this.httpSession = httpSession;
        this.repository = repository;
        this.usersService = usersService;
    }

    @Override
    public Customer save(Customer customer, boolean isCreated) {
        LOG.info("Debut du Process 'save'");
        if (customer != null) {
            Users user;
            if(isCreated)
                user = usersService.save(customer);
            else {
                user = customer.getUser();
                usersService.save(user,true,customer);
            }
            if (user == null)
                return null;
            return customer;
        }
        return null;
    }

    @Override
    public List<Customer> find() {
        LOG.info("Debut du Process 'find'");
        return repository.findAll();
    }

    @Override
    public Customer find(long id) {
        LOG.info("Debut du Process 'find'");
        return repository.findById(id).orElse(null);
    }

    @Override
    public boolean delete(long id) {
        LOG.info("Debut du Process 'delete'");
        LOG.info("Address IP qui Execute le Process " + httpSession.getAttribute("IP"));
        Customer customer = find(id);
        if (customer != null) {
            Users user = customer.getUser();
            if (user != null) {
                long userId = user.getId();
                return usersService.delete(userId, Profile.Role.CUSTOMER);
            }
        }
        return false;
    }
}
