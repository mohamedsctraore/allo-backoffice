package com.alloallo.backoffice.core.service.impl;

import com.alloallo.backoffice.core.entity.Users;
import com.alloallo.backoffice.core.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * @author Christian Amani on 20/10/2018.
 */
@Service
public class ProfileDetailsService implements UserDetailsService {

    private final Logger LOG = LoggerFactory.getLogger(ProfileDetailsService.class);

    private final UserRepository repository;

    @Autowired
    public ProfileDetailsService(UserRepository repository) {
        this.repository = repository;
    }


    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        LOG.info("Debut du Process 'loadUserByUsername'");
        if (s != null) {
            Users users = repository.findByLogin(s);
            if (users != null)
                return users;
            throw new UsernameNotFoundException("Administrator is not exist");
        }
        throw new UsernameNotFoundException("Username is null");
    }
}
