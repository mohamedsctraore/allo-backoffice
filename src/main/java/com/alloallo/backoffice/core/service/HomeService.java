package com.alloallo.backoffice.core.service;

import java.util.Map;

/**
 * @author  Christian Amani on 22/10/2018.
 */
public interface HomeService {

    Map<String, Object> addInformationSummaryToModel();
}
