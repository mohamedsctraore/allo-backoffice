package com.alloallo.backoffice.core.service;

import com.alloallo.backoffice.core.entity.Administrator;
import com.alloallo.backoffice.core.entity.Category;
import org.springframework.ui.Model;

import java.util.List;

/**
 * @author Christian Amani on 23/10/2018.
 */
public interface CategoryService {

    Category save(Category category);

    Category find(long id);

    List<Category> find();

    long count(Administrator administrator);

    boolean delete(long id);

    void verifyDataDetail(Model model);

    //void removeDataDetail(Model model);
}
