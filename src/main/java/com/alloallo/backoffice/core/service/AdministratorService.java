package com.alloallo.backoffice.core.service;

import com.alloallo.backoffice.core.entity.Administrator;
import com.alloallo.backoffice.core.entity.Users;

import java.util.List;

/**
 * @author Christian Amani on 20/10/2018.
 */
public interface AdministratorService {

    Administrator save(Administrator administrator);

    Administrator find(Users user);

    List<Administrator> find(Administrator parent);

    long count(Administrator parent);
}
