package com.alloallo.backoffice.core.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.MappedSuperclass;

/**
 * @author  Christian Amani on 20/10/2018.
 */
@Embeddable
public class LocalizationCoordinate {

    @Column(name = "longitude")
    private double lng;

    @Column(name = "latitude")
    private double lat;

    public LocalizationCoordinate() {
    }

    public double getLng() {
        return lng;
    }

    public void setLng(long lng) {
        this.lng = lng;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(long lat) {
        this.lat = lat;
    }
}
