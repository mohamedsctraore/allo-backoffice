package com.alloallo.backoffice.core.model;

import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author Christian Amani on 20/10/2018.
 */
public class OpinionPK implements Serializable{
    @Id
    private long customer;

    @Id
    private long artisan;

    @Override
    public int hashCode() {
        return Objects.hash(customer, artisan);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || this.getClass() != obj.getClass())
            return false;
        OpinionPK opinionPK = (OpinionPK) obj;
        return Objects.equals(artisan, opinionPK.artisan) &&
                Objects.equals(customer, opinionPK.customer);
    }
}
