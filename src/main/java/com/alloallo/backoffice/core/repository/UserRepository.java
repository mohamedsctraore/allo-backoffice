package com.alloallo.backoffice.core.repository;

import com.alloallo.backoffice.core.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author  Christian Amani on 20/10/2018.
 */
@Repository
public interface UserRepository  extends JpaRepository<Users,Long>{

    Users findByLogin(String login);

    boolean existsByLogin(String login);
}
