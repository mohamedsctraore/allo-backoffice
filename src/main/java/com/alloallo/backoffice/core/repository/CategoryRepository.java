package com.alloallo.backoffice.core.repository;

import com.alloallo.backoffice.core.entity.Administrator;
import com.alloallo.backoffice.core.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Christian Amani on 20/10/2018.
 */
@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

    long countByAdministrator(Administrator administrator);
}
