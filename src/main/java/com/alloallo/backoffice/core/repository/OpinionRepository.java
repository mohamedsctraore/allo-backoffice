package com.alloallo.backoffice.core.repository;

import com.alloallo.backoffice.core.entity.Opinion;
import com.alloallo.backoffice.core.model.OpinionPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Christian Amani on 20/10/2018.
 */
@Repository
public interface OpinionRepository extends JpaRepository<Opinion, OpinionPK> {
}
