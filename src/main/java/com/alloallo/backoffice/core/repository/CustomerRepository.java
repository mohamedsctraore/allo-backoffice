package com.alloallo.backoffice.core.repository;

import com.alloallo.backoffice.core.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Christian Amani on 20/10/2018.
 */
@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

    void deleteByUser_Id(long id);
}
