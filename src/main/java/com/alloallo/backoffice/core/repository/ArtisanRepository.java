package com.alloallo.backoffice.core.repository;

import com.alloallo.backoffice.core.entity.Administrator;
import com.alloallo.backoffice.core.entity.Artisan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Christian Amani on 20/10/2018.
 */
@Repository
public interface ArtisanRepository extends JpaRepository<Artisan,Long> {

    long countByAdministrator(Administrator administrator);

    void deleteByUser_Id(long id);
}
