package com.alloallo.backoffice.core.repository;

import com.alloallo.backoffice.core.entity.Administrator;
import com.alloallo.backoffice.core.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Christian Amani on 20/10/2018.
 */
@Repository
public interface AdministratorRepository extends JpaRepository<Administrator,Long> {

    Administrator findByUser(Users user);

    List<Administrator> findByUser_Parent(Users parent);

    long countByUser_Parent(Users parent);

    void deleteByUser_Id(long id);
}
