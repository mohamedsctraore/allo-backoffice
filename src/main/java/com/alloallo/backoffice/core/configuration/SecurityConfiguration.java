package com.alloallo.backoffice.core.configuration;

import com.alloallo.backoffice.core.service.impl.ProfileDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

/**
 * @author Christian Amani on 20/10/2018.
 */
@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {


    private final PasswordEncoder passwordEncoder;
    private final ProfileLogoutSuccessHandler accountLogoutSuccessHandler;
    private final ProfileAuthenticationSuccessHandler profileAuthenticationSuccessHandler;
    private final ProfileDetailsService profileDetailsService;

    @Autowired
    public SecurityConfiguration(PasswordEncoder passwordEncoder, ProfileLogoutSuccessHandler accountLogoutSuccessHandler
            , ProfileAuthenticationSuccessHandler profileAuthenticationSuccessHandler, ProfileDetailsService profileDetailsService) {
        this.passwordEncoder = passwordEncoder;
        this.accountLogoutSuccessHandler = accountLogoutSuccessHandler;
        this.profileAuthenticationSuccessHandler = profileAuthenticationSuccessHandler;
        this.profileDetailsService = profileDetailsService;
    }


    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setPasswordEncoder(passwordEncoder);
        authenticationProvider.setUserDetailsService(profileDetailsService);
        return authenticationProvider;
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf()
                .disable()
                .logout()
                .permitAll()
                .logoutSuccessUrl("/login")
                .logoutSuccessHandler(accountLogoutSuccessHandler)
                .and()
                .formLogin()
                .permitAll()
                .usernameParameter("login")
                .passwordParameter("password")
                .loginPage("/login")
                .successHandler(profileAuthenticationSuccessHandler)
                .and()
                .authorizeRequests()
                .antMatchers("/registration")
                .permitAll()
                .anyRequest()
                .authenticated();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/", "/*.html", "/**/*.html", "/**/*.css","/*.min.js","/**/*.min.js", "/**/*.js", "/**/*.png", "/**/*.jpg",
                "/**/*.gif", "/**/*.svg", "/**/*.ico", "/**/*.ttf", "/**/*.woff", "/**/*.min.css");
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder authenticationManagerBuilder) {
        authenticationManagerBuilder.authenticationProvider(authenticationProvider());
    }
}
