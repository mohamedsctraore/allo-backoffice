package com.alloallo.backoffice.core.configuration;

import com.alloallo.backoffice.core.entity.Users;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * @author  Christian Amani on 20/10/2018.
 */
@Component
public class ProfileLogoutSuccessHandler implements LogoutSuccessHandler,UtilsHandler{

    private final Logger LOG = LoggerFactory.getLogger(ProfileLogoutSuccessHandler.class);
    private final HttpSession httpSession;

    @Autowired
    public ProfileLogoutSuccessHandler(HttpSession httpSession) {
        this.httpSession = httpSession;
    }

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        LOG.debug("Début du Process 'onLogoutSuccess'");
        LOG.info("Succès de la déconnexion");
        LOG.info("Remote IP : "+request.getRemoteAddr());
        Users principal = (Users) authentication.getPrincipal();
        if(principal != null) {
            LOG.info("Nom : "+principal.getUsername());
            String authorities = getAuthorities(principal);
            LOG.info("Privilege : "+authorities);
        }
        httpSession.removeAttribute("user");
        httpSession.removeAttribute("userObject");
        response.sendRedirect("/login");
    }

    @Override
    public String getAuthorities(Users users) {
        LOG.info("Début du Process 'getAuthorities'");
        Collection<? extends GrantedAuthority> grantedAuthority = users.getAuthorities();
        return AuthorityUtils.authorityListToSet(grantedAuthority)
                .stream()
                .collect(Collectors.joining(","));
    }
}
