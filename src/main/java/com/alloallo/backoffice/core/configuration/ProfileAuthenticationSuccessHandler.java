package com.alloallo.backoffice.core.configuration;

import com.alloallo.backoffice.core.entity.Users;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * @author Christian Amani on 24/08/2018.
 */
@Component
public class ProfileAuthenticationSuccessHandler implements AuthenticationSuccessHandler, UtilsHandler {

    private Logger LOG = LoggerFactory.getLogger(ProfileAuthenticationSuccessHandler.class);
    private final HttpSession httpSession;

    @Autowired
    public ProfileAuthenticationSuccessHandler(HttpSession httpSession) {
        this.httpSession = httpSession;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        LOG.debug("Début du Process 'onAuthenticationSuccess'");
        LOG.info("Succès de l'authentification");
        String remoteAddr = request.getRemoteAddr();
        LOG.info("Remote IP : " + remoteAddr);
        httpSession.setAttribute("IP", remoteAddr);
        Users principal = (Users) authentication.getPrincipal();
        if (principal != null) {
            httpSession.setAttribute("user",principal.getFirstName() +" "+principal.getLastName());
            httpSession.setAttribute("userObject",principal);
            LOG.info("Nom : " + principal.getUsername());
            String authorities = getAuthorities(principal);
            LOG.info("Privilege : " + authorities);
        } else
            httpSession.setAttribute("user","Aucun n'utilisateur connecté");
        response.sendRedirect("/home");
    }

    @Override
    public String getAuthorities(Users users) {
        LOG.info("Début du Process 'getAuthorities'");
        Collection<? extends GrantedAuthority> grantedAuthority = users.getAuthorities();
        return AuthorityUtils.authorityListToSet(grantedAuthority)
                .stream()
                .collect(Collectors.joining(","));
    }
}
