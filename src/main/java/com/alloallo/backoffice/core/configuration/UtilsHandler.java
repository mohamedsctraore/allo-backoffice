package com.alloallo.backoffice.core.configuration;


import com.alloallo.backoffice.core.entity.Users;

/**
 * @author  Christian Amani on 20/10/2018.
 */
public interface UtilsHandler {

    String getAuthorities(Users users);
}
