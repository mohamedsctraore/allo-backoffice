package com.alloallo.backoffice.core.entity;

import com.alloallo.backoffice.core.model.OpinionPK;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * @author Christian Amani on 20/10/2018.
 */
@Table
@Entity
@IdClass(OpinionPK.class)
public class Opinion implements Serializable{

    @Id
    @ManyToOne
    private Customer customer;

    @Id
    @ManyToOne
    private Artisan artisan;

    private long stars;

    private String description;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime created;

    public Opinion() {
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Artisan getArtisan() {
        return artisan;
    }

    public void setArtisan(Artisan artisan) {
        this.artisan = artisan;
    }

    public long getStars() {
        return stars;
    }

    public void setStars(long stars) {
        this.stars = stars;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj)
            return true;
        if(obj == null ||  getClass() != obj.getClass())
            return false;
        Opinion opinion = (Opinion) obj;
        return Objects.equals(artisan,opinion.artisan) &&
                Objects.equals(customer,opinion.customer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(customer,artisan,description,stars);
    }
}
