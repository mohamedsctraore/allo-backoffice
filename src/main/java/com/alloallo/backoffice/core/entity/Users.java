package com.alloallo.backoffice.core.entity;

import com.alloallo.backoffice.core.model.Profile;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Collection;

/**
 * @author Christian Amani on 20/10/2018.
 */
@Table
@Entity
public class Users extends Profile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate birthDate;

    @Column(nullable = false)
    private Gender gender;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private Privilege privilege;

    @ManyToOne
    @JoinColumn
    private Users parent;

    public Users() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    @JsonIgnore
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return AuthorityUtils.createAuthorityList(privilege.value);
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Privilege getPrivilege() {
        return privilege;
    }

    public void setPrivilege(Privilege privilege) {
        this.privilege = privilege;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFullName() {
        return firstName + " " + lastName;
    }

    public Users getParent() {
        return parent;
    }

    public void setParent(Users parent) {
        this.parent = parent;
    }

    public enum Gender {
        MAL,
        FEMALE;

        public String isGender() {
            if (this == MAL)
                return "Homme";
            else
                return "FEMME";
        }
    }

    public  enum Privilege {
        CUSTOMER("CUSTOMER"),
        ARTISAN("ARTISAN"),
        ADMIN_SMALL("ADMIN_SMALL"),
        ADMIN_MEDIUM("ADMIN_MEDIUM"),
        ADMIN_BIG("ADMIN_BIG");

        Privilege(String value) {
            this.value = value;
        }


        private String value;

        @Override
        public String toString() {
            return value;
        }
    }

}
