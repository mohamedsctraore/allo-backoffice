package com.alloallo.backoffice.core.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Christian Amani on 20/10/2018.
 */
@Table
@Entity
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String libelle;

    private String description;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate updated;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "administrator_id",nullable = false)
    private Administrator administrator;

    @OneToMany(mappedBy = "category",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private List<Artisan> artisans = new ArrayList<>();

    public Category() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDate updated) {
        this.updated = updated;
    }

    public Administrator getAdministrator() {
        return administrator;
    }

    public void setAdministrator(Administrator administrator) {
        this.administrator = administrator;
    }


    public Artisan addArtisan(Artisan artisan) {
        if(artisan != null) {
            artisan.setCategory(this);
            artisans.add(artisan);
            return artisan;
        }
        return null;
    }

    public Artisan removeArtisan(Artisan artisan) {
        if(artisan != null) {
            artisan.setCategory(null);
            artisans.remove(artisan);
            return artisan;
        }
        return null;
    }

    public List<Artisan> getArtisans() {
        return artisans;
    }

    public void setArtisans(List<Artisan> artisans) {
        this.artisans = artisans;
    }
}
