package com.alloallo.backoffice.core.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Christian Amani on 20/10/2018.
 */
@Table
@Entity
public class Administrator {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToOne()
    @JoinColumn(name = "user_id")
    private Users user;

    @OneToMany(mappedBy = "administrator")
    private List<Category> categories = new ArrayList<>();

    public Administrator() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public Category addCategory(Category category) {
        if(category != null) {
            category.setAdministrator(this);
            categories.add(category);
            return category;
        }
        return null;
    }

    public Category removeCategory(Category category) {
        if(category != null) {
            category.setAdministrator(null);
            categories.remove(category);
            return category;
        }
        return null;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

}
