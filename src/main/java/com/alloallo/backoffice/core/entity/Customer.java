package com.alloallo.backoffice.core.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Christian Amani on 20/10/2018.
 */
@Table
@Entity
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToOne()
    @JoinColumn(name = "user_id")
    private Users user;

    @OneToMany(mappedBy = "customer")
    private List<Opinion> opinions = new ArrayList<>();

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<Artisan> artisans = new ArrayList<>();

    public Customer() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public Opinion addOpinion(Opinion opinion) {
        if(opinion != null) {
            opinion.setCustomer(this);
            opinions.add(opinion);
            return opinion;
        }
        return null;
    }

    public Opinion removeOpinion(Opinion opinion) {
        if(opinion != null) {
            opinion.setCustomer(null);
            opinions.remove(opinion);
            return opinion;
        }
        return null;
    }

    public List<Opinion> getOpinions() {
        return opinions;
    }

    public void setOpinions(List<Opinion> opinions) {
        this.opinions = opinions;
    }

    public Artisan addArtisan(Artisan artisan) {
        if(artisan != null && artisans.size() < 5) {
            artisans.add(artisan);
            return artisan;
        }
        return artisan;
    }

    public Artisan removeArtisan(Artisan artisan) {
        if(artisan != null) {
            artisans.remove(artisan);
            return artisan;
        }
        return null;
    }

    public List<Artisan> getArtisans() {
        return artisans;
    }

    public void setArtisans(List<Artisan> artisans) {
        this.artisans = artisans;
    }
}
