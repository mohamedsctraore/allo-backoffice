package com.alloallo.backoffice.core.entity;

import com.alloallo.backoffice.core.model.LocalizationCoordinate;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Christian Amani on 20/10/2018.
 */
@Table
@Entity
public class Artisan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String descriptionLocalisation;

    private LocalizationCoordinate localizationCoordinate;

    @OneToOne()
    @JoinColumn(name = "user_id")
    private Users user;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.PERSIST
            , CascadeType.REFRESH})
    @JoinColumn(name = "category_id")
    private Category category;

    @OneToMany(mappedBy = "artisan", cascade = CascadeType.ALL)
    private List<Opinion> opinions = new ArrayList<>();

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.PERSIST
            , CascadeType.REFRESH})
    @JoinColumn()
    private Administrator administrator;


    public Artisan() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public String getDescriptionLocalisation() {
        return descriptionLocalisation;
    }

    public void setDescriptionLocalisation(String descriptionLocalisation) {
        this.descriptionLocalisation = descriptionLocalisation;
    }

    public LocalizationCoordinate getLocalizationCoordinate() {
        return localizationCoordinate;
    }

    public void setLocalizationCoordinate(LocalizationCoordinate localizationCoordinate) {
        this.localizationCoordinate = localizationCoordinate;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Administrator getAdministrator() {
        return administrator;
    }

    public void setAdministrator(Administrator administrator) {
        this.administrator = administrator;
    }

    public Opinion addOpinion(Opinion opinion) {
        if (opinion != null) {
            opinion.setArtisan(this);
            opinions.add(opinion);
            return opinion;
        }
        return null;
    }

    public Opinion removeOpinion(Opinion opinion) {
        if (opinion != null) {
            opinion.setArtisan(null);
            opinions.remove(opinion);
            return opinion;
        }
        return null;
    }

    public List<Opinion> getOpinions() {
        return opinions;
    }

    public void setOpinions(List<Opinion> opinions) {
        this.opinions = opinions;
    }
}
