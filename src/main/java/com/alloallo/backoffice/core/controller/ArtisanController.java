package com.alloallo.backoffice.core.controller;

import com.alloallo.backoffice.core.entity.Artisan;
import com.alloallo.backoffice.core.entity.Category;
import com.alloallo.backoffice.core.entity.Opinion;
import com.alloallo.backoffice.core.entity.Users;
import com.alloallo.backoffice.core.service.ArtisanService;
import com.alloallo.backoffice.core.service.CategoryService;
import com.alloallo.backoffice.core.service.OpinionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Christian Amani on 01/11/2018.
 */
@Controller
public class ArtisanController {

    private final Logger LOG = LoggerFactory.getLogger(ArtisanController.class);
    private final ArtisanService artisanService;
    private final CategoryService categoryService;
    private final OpinionService opinionService;

    @Autowired
    public ArtisanController(ArtisanService artisanService, CategoryService categoryService
            , OpinionService opinionService) {
        this.artisanService = artisanService;
        this.categoryService = categoryService;
        this.opinionService = opinionService;
    }

    @GetMapping("/artisans")
    public String artisansPage(Model model) {
        LOG.info("Debut du Process 'artisansPage'");
        Artisan artisan = getNewArtisanForModel();
        model.addAttribute("newArtisan", artisan);
        model.addAttribute("mal", Users.Gender.MAL);
        model.addAttribute("female", Users.Gender.FEMALE);
        List<Artisan> artisans = artisanService.find();
        model.addAttribute("artisans", artisans);
        List<Category> categories = categoryService.find();
        model.addAttribute("categories", categories);
        List<Opinion> opinions = opinionService.find();
        model.addAttribute("opinions", opinions);
        return "artisan/container";
    }

    @GetMapping("/artisan/{id}")
    public String artisanDetailPage(Model model, @PathVariable long id, RedirectAttributes redirectAttributes) {
        LOG.info("Debut du Process 'artisanDetailPage'");
        Artisan artisan = artisanService.find(id);
        if (artisan == null) {
            redirectAttributes.addFlashAttribute("findItISError");
            return "redirect:/artisan";
        }
        model.addAttribute("detailsArtisan", artisan);
        List<Category> categories = categoryService.find();
        model.addAttribute("categories", categories);
        model.addAttribute("mal", Users.Gender.MAL);
        model.addAttribute("female", Users.Gender.FEMALE);
        return "artisan/form_details";
    }

    @PostMapping("/artisan")
    public String postArtisanForm(@Valid Artisan artisan, BindingResult bindingResult
            , RedirectAttributes redirectAttributes) {
        LOG.info("Debut du Process 'postArtisanForm'");
        artisan.getUser().setPrivilege(Users.Privilege.ARTISAN);
        artisan.getUser().setRole(Users.Role.ARTISAN);
        if (bindingResult.hasErrors())
            redirectAttributes.addFlashAttribute("saveItIsError", false);
        else {
            artisan = artisanService.save(artisan, true);
            if (artisan != null)
                redirectAttributes.addAttribute("saveItIsSuccess", true);
            else
                redirectAttributes.addFlashAttribute("saveItIsError", false);
        }
        return "redirect:/artisans";
    }

    @PostMapping("/artisan/updated")
    public String putArtisanForm(@Valid Artisan artisan, BindingResult bindingResult
            , RedirectAttributes redirectAttributes) {
        LOG.info("Debut du Process 'putArtisanForm'");
        if (bindingResult.hasErrors())
            redirectAttributes.addFlashAttribute("saveItIsSuccess", false);
        else {
            artisan = artisanService.save(artisan, false);
            if (artisan == null)
                redirectAttributes.addAttribute("saveItIsError", true);
            else
                redirectAttributes.addAttribute("saveItIsSuccess", true);
        }
        return "redirect:/artisans";
    }


    @DeleteMapping("/artisan/delete/{id}")
    public ResponseEntity<Boolean> deleteArtisan(@PathVariable long id) {
        LOG.info("Debut du Process 'deleteArtisan'");
        boolean isDeleted = artisanService.delete(id);
        return new ResponseEntity<>(isDeleted, HttpStatus.OK);
    }

    private Artisan getNewArtisanForModel() {
        Artisan artisan = new Artisan();
        artisan.setCategory(new Category());
        Users user = new Users();
        artisan.setUser(user);
        return artisan;
    }
}
