package com.alloallo.backoffice.core.controller;

import com.alloallo.backoffice.core.service.OpinionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Christian Amani on 16/11/2018.
 */
@Controller
public class OpinionController {

    private final Logger LOG = LoggerFactory.getLogger(OpinionController.class);

    private final OpinionService opinionService;

    @Autowired
    public OpinionController(OpinionService opinionService) {
        this.opinionService = opinionService;
    }


    @GetMapping("/opinions/satisfaction/average/chart")
    public ResponseEntity<Map<String, Double>> satisfactionAverageDataChart() {
        LOG.info("Debut du Process 'satisfactionDataChart'");
        Map<String,Double> satisfactionData = new HashMap<>();
        double averageDissatisfied = opinionService.findAverageDissatisfied();
        satisfactionData.put("dissatisfied",averageDissatisfied);
        double averageSatisfied = opinionService.findAverageSatisfied();
        satisfactionData.put("satisfied",averageSatisfied);
        return new ResponseEntity<>(satisfactionData, HttpStatus.OK);
    }
}
