package com.alloallo.backoffice.core.controller;

import com.alloallo.backoffice.core.entity.Artisan;
import com.alloallo.backoffice.core.entity.Customer;
import com.alloallo.backoffice.core.entity.Opinion;
import com.alloallo.backoffice.core.entity.Users;
import com.alloallo.backoffice.core.model.Profile;
import com.alloallo.backoffice.core.service.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Christian Amani on 06/11/2018.
 */
@Controller
public class CustomerController {

    private final Logger LOG = LoggerFactory.getLogger(CustomerController.class);
    private final CustomerService customerService;


    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/customers")
    public String customersPage(Model model) {
        LOG.info("Debut du Process 'customersPage'");
        Customer customer = new Customer();
        customer.setUser(new Users());
        model.addAttribute("newCustomer", customer);
        model.addAttribute("mal", Users.Gender.MAL);
        model.addAttribute("female", Users.Gender.FEMALE);
        List<Customer> customers = customerService.find();
        model.addAttribute("customers", customers);
        return "client/container";
    }

    @GetMapping("/customer/{id}")
    public String customerDetailPage(Model model, @PathVariable long id, RedirectAttributes redirectAttributes) {
        LOG.info("Debut du Process 'customerDetailPage'");
        Customer customer = customerService.find(id);
        if (customer == null) {
            redirectAttributes.addFlashAttribute("findItISError");
            return "redirect:/customers";
        }
        List<Artisan> artisans = customer.getArtisans();
        model.addAttribute("artisanStart", artisans);
        List<Opinion> opinions = customer.getOpinions();
        model.addAttribute("opinions", opinions);
        model.addAttribute("detailsCustomer", customer);
        model.addAttribute("mal", Users.Gender.MAL);
        model.addAttribute("female", Users.Gender.FEMALE);
        return "client/form_details";
    }

    @PostMapping("/customer")
    public String postCustomerForm(@Valid Customer customer, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        LOG.info("Debut du Process 'postCustomerForm'");
        if (bindingResult.hasErrors())
            redirectAttributes.addFlashAttribute("saveItIsError", true);
        else {
            customer = customerService.save(customer, true);
            if (customer != null)
                redirectAttributes.addAttribute("saveItIsSuccess", true);
            else
                redirectAttributes.addFlashAttribute("saveItIsError", false);
        }
        return "redirect:/customers";
    }

    @PostMapping("/customer/updated")
    public String putCustomerForm(@Valid Customer customer, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        LOG.info("Debut du Process 'putCustomerForm'");
        if (bindingResult.hasErrors())
            redirectAttributes.addFlashAttribute("saveItIsError", true);
        else {
            customer = customerService.save(customer, false);
            if (customer == null)
                redirectAttributes.addAttribute("saveItIsError", true);
            else
                redirectAttributes.addAttribute("saveItIsSuccess", true);
        }
        return "redirect:/customers";
    }

    @DeleteMapping("/customer/delete/{id}")
    public ResponseEntity<Boolean> deleteCustomer(@PathVariable long id) {
        LOG.info("Debut du Process 'deleteCustomer'");
        boolean isDeleted = customerService.delete(id);
        return new ResponseEntity<>(isDeleted, HttpStatus.OK);
    }
}
