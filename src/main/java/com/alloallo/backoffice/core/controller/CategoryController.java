package com.alloallo.backoffice.core.controller;

import com.alloallo.backoffice.core.entity.Artisan;
import com.alloallo.backoffice.core.entity.Category;
import com.alloallo.backoffice.core.service.CategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Christian Amani on 23/10/2018.
 */
@Controller
public class CategoryController {

    private final Logger LOG = LoggerFactory.getLogger(CategoryController.class);
    private final CategoryService service;

    @Autowired
    public CategoryController(CategoryService service) {
        this.service = service;
    }

    @GetMapping("/categories")
    public String categoriesPage(Model model) {
        LOG.info("Debut du Process 'categoriesPage'");
        List<Category> categories = service.find();
        model.addAttribute("categories", categories);
        model.addAttribute("formCategory", new Category());
        service.verifyDataDetail(model);
        return "category/container";
    }

    @GetMapping("/categories/{id}")
    public String categoriesDetailPage(@PathVariable long id, RedirectAttributes redirectAttributes) {
        LOG.info("Debut du Proces 'categoriesDetailPage'");
        Category category = service.find(id);
        if (category != null) {
            redirectAttributes.addFlashAttribute("formDetailCategory", category);
            List<Artisan> artisans = category.getArtisans();
            redirectAttributes.addFlashAttribute("categoriesArtisan", artisans);
            redirectAttributes.addFlashAttribute("showingDetail", true);
        }
        return "redirect:/categories";
    }

    @PostMapping("/category")
    public String postCategoryForm(@Valid Category category, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        LOG.info("Debut du Process 'postCategoryForm'");
        if (bindingResult.hasErrors())
            redirectAttributes.addAttribute("saveItIsSuccess", false);
        else {
            service.save(category);
            redirectAttributes.addAttribute("saveItIsSuccess", true);
        }
        return "redirect:/categories";
    }

    @DeleteMapping("/category/delete/{id}")
    public ResponseEntity<Boolean> deleteCategory(@PathVariable long id) {
        LOG.info("Debut du Process 'deleteCategory'");
        boolean isDeleted = service.delete(id);
        return new ResponseEntity<>(isDeleted, HttpStatus.OK);
    }
}
