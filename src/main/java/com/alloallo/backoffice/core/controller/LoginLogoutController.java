package com.alloallo.backoffice.core.controller;

import com.alloallo.backoffice.core.model.Profile;
import com.alloallo.backoffice.core.entity.Users;
import com.alloallo.backoffice.core.service.HomeService;
import com.alloallo.backoffice.core.service.UsersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Map;

/**
 * @author  Christian Amani on 20/10/2018.
 */
@Controller
public class LoginLogoutController {

    private final Logger LOG = LoggerFactory.getLogger(LoginLogoutController.class);
    private final UsersService usersService;
    private final HomeService homeService;

    public LoginLogoutController(UsersService usersService, HomeService homeService) {
        this.usersService = usersService;
        this.homeService = homeService;
    }

    @GetMapping("/login")
    public String loginPage(RedirectAttributes redirectAttributes) {
        LOG.debug("Debut du Process 'loginPage'");
        boolean isEmpty = usersService.find()
                .isEmpty();
        if(isEmpty)
            return "redirect:/registration";
        return "login";
    }

    @GetMapping("/")
    public String indexPage() {
        LOG.debug("Debut du Process 'indexPage'");
        return "redirect:/home";
    }

    @GetMapping("/home")
    public String homePage(Model model) {
        LOG.debug("Debut du Process 'homePage'");
        Map<String, Object> informationSummary = homeService.addInformationSummaryToModel();
        model.addAllAttributes(informationSummary);
        return "dashboard/home";
    }

    @GetMapping("/registration")
    public String registrationPage(Model model) {
        LOG.debug("Debut du Process 'registrationPage'");
        Users user = new Users();
        user.setRole(Profile.Role.ADMIN);
        user.setPrivilege(Users.Privilege.ADMIN_BIG);
        model.addAttribute("user",user);
        model.addAttribute("mal", Users.Gender.MAL);
        model.addAttribute("female", Users.Gender.FEMALE);
        return "registration";
    }

    @PostMapping("/registration")
    public String postRegistrationForm(@Valid Users user, BindingResult bindingResult,RedirectAttributes redirectAttributes) {
        LOG.info("Debut du Process 'postRegistrationForm'");
        if(bindingResult.hasErrors())
            return "redirect:/registration";
        else {
            user = usersService.save(user, true);
            if(user != null) {
                redirectAttributes.addAttribute("isRegistrationWithSuccess",true);
                return "redirect:/login";
            } else {
                redirectAttributes.addAttribute("isRegistrationWithError",true);
                return "redirect:/registration";
            }
        }
    }
}
