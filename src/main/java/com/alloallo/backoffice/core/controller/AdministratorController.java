package com.alloallo.backoffice.core.controller;

import com.alloallo.backoffice.core.entity.Administrator;
import com.alloallo.backoffice.core.entity.Users;
import com.alloallo.backoffice.core.model.Profile;
import com.alloallo.backoffice.core.service.AdministratorService;
import com.alloallo.backoffice.core.service.ArtisanService;
import com.alloallo.backoffice.core.service.CategoryService;
import com.alloallo.backoffice.core.service.UsersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Collections;
import java.util.List;

/**
 * @author Christian Amani on 10/12/2018.
 */
@Controller
public class AdministratorController {

    private final Logger LOG = LoggerFactory.getLogger(AdministratorController.class);

    private final UsersService usersService;
    private final AdministratorService administratorService;
    private final CategoryService categoryService;
    private final ArtisanService artisanService;
    private final HttpSession httpSession;
    private final Users.Privilege ADMIN_SMALL = Users.Privilege.ADMIN_SMALL;
    private final Users.Privilege ADMIN_MEDIUM = Users.Privilege.ADMIN_MEDIUM;
    private final Users.Privilege ADMIN_BIG = Users.Privilege.ADMIN_BIG;

    @Autowired
    public AdministratorController(UsersService usersService, AdministratorService administratorService
            , CategoryService categoryService, ArtisanService artisanService, HttpSession httpSession) {
        this.usersService = usersService;
        this.administratorService = administratorService;
        this.categoryService = categoryService;
        this.artisanService = artisanService;
        this.httpSession = httpSession;
    }

    @GetMapping("/administrators")
    public String administratorPage(Model model) {
        LOG.info("Debut du Process 'administratorPage'");
        Users user = (Users) httpSession.getAttribute("userObject");
        Administrator administrator = administratorService.find(user);
        if (administrator != null) {
            model.addAttribute("admin", administrator);
            long countChildren = administratorService.count(administrator);
            model.addAttribute("children", countChildren);
            long nbrCategories = categoryService.count(administrator);
            model.addAttribute("categories", nbrCategories);
            long nbrArtisans = artisanService.count(administrator);
            model.addAttribute("artisans", nbrArtisans);
            model.addAttribute("mal", Users.Gender.MAL);
            model.addAttribute("female", Users.Gender.FEMALE);
            model.addAttribute("adminSmall", ADMIN_SMALL);
            model.addAttribute("adminMedium", ADMIN_MEDIUM);
            model.addAttribute("adminBig", ADMIN_BIG);
            Administrator newAdmin = new Administrator();
            Users newUser = new Users();
            newUser.setParent(user);
            newUser.setRole(Profile.Role.ADMIN);
            newAdmin.setUser(newUser);
            model.addAttribute("newAdmin", newAdmin);
            return "administrator/container";
        } else
            return "redirect:/";
    }

    @GetMapping("/administrators/children")
    public ResponseEntity<List<Administrator>> findChildrenForCurrentUser() {
        LOG.info("Debut du Process 'findChildrenForCurrentUser'");
        Users users = (Users) httpSession.getAttribute("userObject");
        Administrator administrator = administratorService.find(users);
        if (administrator != null) {
            List<Administrator> administrators = administratorService.find(administrator);
            return new ResponseEntity<>(administrators, HttpStatus.OK);
        } else
            return new ResponseEntity<>(Collections.emptyList(), HttpStatus.OK);
    }

    @PostMapping("/administrators")
    public String putAdministratorForm(@Valid Administrator administrator, BindingResult bindingResult
            , RedirectAttributes redirectAttributes) {
        LOG.info("Debut du Process 'putAdministratorForm'");
        if (bindingResult.hasErrors())
            redirectAttributes.addFlashAttribute("saveItIsSuccess", false);
        else {
            Users user = administrator.getUser();
            long id = user.getId();
            String password = usersService.findPassword(id);
            user.setPassword(password);
            user = usersService.save(user, false, administrator);
            if (user != null)
                redirectAttributes.addFlashAttribute("saveItIsSuccess", true);
            else
                redirectAttributes.addFlashAttribute("saveItIsSuccess", false);
        }
        return "redirect:/administrators";
    }

    @PostMapping("/administrators/security")
    public String putAdministratorSecurityForm(@Valid Administrator administrator, @RequestParam(name = "oldPassword") String oldPassword
            , BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        LOG.info("Debut du Process 'putAdministratorSecurityForm'");
        if (bindingResult.hasErrors())
            redirectAttributes.addFlashAttribute("saveItIsSuccess", false);
        else {
            Users user = administrator.getUser();
            long id = user.getId();
            boolean matched = usersService.verifyPassword(id, oldPassword);
            if (matched) {
                user = usersService.save(user, true, administrator);
                if (user != null)
                    redirectAttributes.addFlashAttribute("saveItIsSuccess", true);
                else
                    redirectAttributes.addFlashAttribute("saveItIsSuccess", false);
            } else
                redirectAttributes.addFlashAttribute("saveItIsSuccess", false);
        }
        return "redirect:/administrators";
    }

    @PostMapping("/administrators/created")
    public String postAdministratorForm(@Valid Administrator administrator, BindingResult bindingResult
            , RedirectAttributes redirectAttributes) {
        LOG.info("Debut du Process 'postAdministratorForm'");
        if (bindingResult.hasErrors())
            redirectAttributes.addFlashAttribute("saveItIsSuccess", false);
        else {
            Users user = usersService.save(administrator);
            if (user != null)
                redirectAttributes.addFlashAttribute("saveItIsSuccess", true);
            else
                redirectAttributes.addFlashAttribute("saveItIsSuccess", false);
        }
        return "redirect:/administrators";
    }

    @PostMapping("administrators/children")
    String postChildrenAdministratorList(@Valid Administrator administrator, BindingResult bindingResult
            , RedirectAttributes redirectAttributes) {
        LOG.info("Debut du Process 'postChildrenAdministratorList'");
        if (bindingResult.hasErrors())
            redirectAttributes.addFlashAttribute("");
        else {
            Users users = usersService.save(administrator);
            if (users != null)
                redirectAttributes.addFlashAttribute("saveItIsSuccess", true);
            else
                redirectAttributes.addFlashAttribute("saveItIsSuccess", false);
        }
        return "redirect:/administrators";
    }

    @PutMapping("administrators/children")
    ResponseEntity<Boolean> putAdministrator(@RequestBody @Valid Administrator administrator) {
        LOG.info("Debut du Process 'putAdministrator'");
        Users user = administrator.getUser();
        if (user != null) {
            long id = user.getId();
            String password = usersService.findPassword(id);
            user.setPassword(password);
            user = usersService.save(user, false, administrator);
            if (user != null)
                return new ResponseEntity<>(true, HttpStatus.OK);
        }
        return new ResponseEntity<>(false, HttpStatus.OK);
    }

    @DeleteMapping("administrators/deleted/{id}")
    ResponseEntity<Boolean> deleteAdministrator(@PathVariable long id) {
        LOG.info("Debut du Process  'deleteAdministrator'");
        Users user = usersService.find(id);
        if(user != null) {
            boolean isDeleted = usersService.delete(id, Profile.Role.ADMIN);
            return new ResponseEntity<>(isDeleted,HttpStatus.OK);
        }
        return new ResponseEntity<>(false,HttpStatus.OK);
    }
}
