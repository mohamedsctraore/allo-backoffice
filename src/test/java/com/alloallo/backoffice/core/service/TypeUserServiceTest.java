package com.alloallo.backoffice.core.service;

import com.alloallo.backoffice.core.entity.Administrator;
import com.alloallo.backoffice.core.entity.Artisan;
import com.alloallo.backoffice.core.entity.Customer;
import com.alloallo.backoffice.core.entity.Users;
import com.alloallo.backoffice.core.model.Profile;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.test.context.junit4.SpringRunner;

import javax.servlet.http.HttpSession;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Christian Amani on 14/12/2018.
 */
@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Service.class))
public class TypeUserServiceTest {

    @MockBean
    private HttpSession httpSession;

    @MockBean
    private PasswordEncoder passwordEncoder;

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private CustomerService customerService;
    @Autowired
    private ArtisanService artisanService;
    @Autowired
    private AdministratorService administratorService;

    private Users user;

    private Customer customer;
    private Artisan artisan;
    private Administrator administrator;


    @Before
    public void setup() {
        Mockito.when(httpSession.getAttribute("IP"))
                .thenReturn("IP : 220.168.123");
        Mockito.when(passwordEncoder.encode(Mockito.anyString()))
                .thenReturn("passwordEncoded");
        Mockito.when(passwordEncoder.matches(Mockito.anyString(), Mockito.anyString()))
                .thenReturn(true);

        user = new Users();
        user.setLogin("simple_admin");
        user.setPassword("password");
        user.setFirstName("Tashia");
        user.setLastName("Cleo");
        user.setCreated(LocalDate.now());
        user.setEmail("cloeTahshia@gmail.com");
        user.setPhoneNumber("+225XXXXXXXX");
        user.setGender(Users.Gender.FEMALE);

        Users userCustomer = new Users();
        userCustomer.setLogin("customerLogin");
        userCustomer.setPassword("customerPassword");
        userCustomer.setFirstName("Bolos");
        userCustomer.setLastName("Dalas");
        userCustomer.setCreated(LocalDate.now());
        userCustomer.setEmail("Bolos@gmail.com");
        userCustomer.setPhoneNumber("+225XXXXXXXX");
        userCustomer.setGender(Users.Gender.MAL);
        userCustomer.setRole(Profile.Role.CUSTOMER);
        userCustomer.setPrivilege(Users.Privilege.CUSTOMER);
        userCustomer = testEntityManager.persist(userCustomer);

        customer = new Customer();
        customer.setUser(userCustomer);

        Users userArtisan = new Users();
        userArtisan.setLogin("artisanLogin");
        userArtisan.setLogin("customerLogin");
        userArtisan.setPassword("customerPassword");
        userArtisan.setFirstName("Bado");
        userArtisan.setLastName("Dalas");
        userArtisan.setCreated(LocalDate.now());
        userArtisan.setEmail("Bolos@gmail.com");
        userArtisan.setPhoneNumber("+225XXXXXXXX");
        userArtisan.setGender(Users.Gender.MAL);
        userArtisan.setRole(Profile.Role.CUSTOMER);
        userArtisan.setPrivilege(Users.Privilege.CUSTOMER);
        userArtisan = testEntityManager.persist(userArtisan);

        artisan = new Artisan();
        artisan.setUser(userArtisan);

        Users userAdmin = new Users();
        userAdmin.setLogin("artisanLogin");
        userAdmin.setLogin("customerLogin");
        userAdmin.setPassword("customerPassword");
        userAdmin.setFirstName("polo");
        userAdmin.setLastName("Dalas");
        userAdmin.setCreated(LocalDate.now());
        userAdmin.setEmail("Bolos@gmail.com");
        userAdmin.setPhoneNumber("+225XXXXXXXX");
        userAdmin.setGender(Users.Gender.MAL);
        userAdmin.setRole(Profile.Role.ADMIN);
        userAdmin.setPrivilege(Users.Privilege.ADMIN_SMALL);
        userAdmin = testEntityManager.persist(userArtisan);

        administrator = new Administrator();
        administrator.setUser(userAdmin);
    }

    @Test
    public void createdCustomer() {
        customer = customerService.save(customer, true);
        assertNotNull(customer);
    }

    @Test
    public void updateCustomer() {
        customer = customerService.save(customer, true);
        assertNotNull(customer);
        Users user = customer.getUser();
        user.setFirstName("Dolos");
        customer = customerService.save(customer, false);
        assertNotNull(customer);
        assertEquals("Dolos", user.getFirstName());
    }

    @Test
    public void saveArtisan() {
        artisan = artisanService.save(artisan, true);
        assertNotNull(artisan);
    }

    @Test
    public void updateArtisan() {
        artisan = artisanService.save(artisan, true);
        assertNotNull(artisan);
        Users user = artisan.getUser();
        user.setFirstName("Bolo");
        artisan = artisanService.save(artisan, false);
        assertNotNull(artisan);
        assertEquals("Bolo", user.getFirstName());
    }

    @Test
    public void saveAdministrator() {
        administrator = administratorService.save(administrator);
        assertNotNull(administrator);
    }

    @Test
    public void updateAdministrator() {
        administrator = administratorService.save(administrator);
        assertNotNull(administrator);
        Users user = administrator.getUser();
        user.setFirstName("Pala");
        administrator = administratorService.save(administrator);
        assertNotNull(administrator);
        assertEquals("Pala", user.getFirstName());
    }

    @Test
    public void findCustomers() {
        Customer customerA = new Customer();
        Users userCustomerA = new Users();
        userCustomerA.setLogin("customerLogin");
        userCustomerA.setPassword("customerPassword");
        userCustomerA.setFirstName("Bolos");
        userCustomerA.setLastName("Dalas");
        userCustomerA.setCreated(LocalDate.now());
        userCustomerA.setEmail("Bolos@gmail.com");
        userCustomerA.setPhoneNumber("+225XXXXXXXX");
        userCustomerA.setGender(Users.Gender.MAL);
        userCustomerA.setRole(Profile.Role.CUSTOMER);
        userCustomerA.setPrivilege(Users.Privilege.CUSTOMER);
        userCustomerA = testEntityManager.persist(userCustomerA);
        customerA.setUser(userCustomerA);
        testEntityManager.persist(customerA);


        Customer customerB = new Customer();
        Users userCustomerB = new Users();
        userCustomerB.setLogin("artisanLogin");
        userCustomerB.setLogin("customerLogin");
        userCustomerB.setPassword("customerPassword");
        userCustomerB.setFirstName("polo");
        userCustomerB.setLastName("Dalas");
        userCustomerB.setCreated(LocalDate.now());
        userCustomerB.setEmail("Bolos@gmail.com");
        userCustomerB.setPhoneNumber("+225XXXXXXXX");
        userCustomerB.setGender(Users.Gender.MAL);
        userCustomerB.setRole(Profile.Role.CUSTOMER);
        userCustomerB.setPrivilege(Users.Privilege.CUSTOMER);
        userCustomerB = testEntityManager.persist(userCustomerB);
        customerB.setUser(userCustomerB);
        testEntityManager.persist(customerB);

        List<Customer> customers = customerService.find();
        assertFalse(customers.isEmpty());
        assertEquals(2, customers.size());
    }

    @Test
    public void findCustomer() {
        Customer customer = new Customer();
        Users userCustomer = new Users();
        userCustomer.setLogin("customerLogin");
        userCustomer.setPassword("customerPassword");
        userCustomer.setFirstName("Bolos");
        userCustomer.setLastName("Dalas");
        userCustomer.setCreated(LocalDate.now());
        userCustomer.setEmail("Bolos@gmail.com");
        userCustomer.setPhoneNumber("+225XXXXXXXX");
        userCustomer.setGender(Users.Gender.MAL);
        userCustomer.setRole(Profile.Role.CUSTOMER);
        userCustomer.setPrivilege(Users.Privilege.CUSTOMER);
        userCustomer = testEntityManager.persist(userCustomer);
        customer.setUser(userCustomer);
        long id = (long) testEntityManager.persistAndGetId(customer);
        customer = customerService.find(id);
        assertNotNull(customer);
    }

    @Test
    public void findAdministrators() {

        Users parent = new Users();
        parent.setLogin("adminLoginA");
        parent.setPassword("customerPassword");
        parent.setFirstName("Bolos");
        parent.setLastName("Klos");
        parent.setCreated(LocalDate.now());
        parent.setEmail("Klos@gmail.com");
        parent.setPhoneNumber("+225XXXXXXXX");
        parent.setGender(Users.Gender.MAL);
        parent.setRole(Profile.Role.ADMIN);
        parent.setPrivilege(Users.Privilege.ADMIN_BIG);
        parent = testEntityManager.persist(parent);
        Administrator root = new Administrator();
        root.setUser(parent);

        Administrator administratorA = new Administrator();
        Users userAdministratorA = new Users();
        userAdministratorA.setLogin("adminLoginA");
        userAdministratorA.setPassword("customerPassword");
        userAdministratorA.setFirstName("Bolos");
        userAdministratorA.setLastName("Klos");
        userAdministratorA.setCreated(LocalDate.now());
        userAdministratorA.setEmail("Klos@gmail.com");
        userAdministratorA.setPhoneNumber("+225XXXXXXXX");
        userAdministratorA.setGender(Users.Gender.MAL);
        userAdministratorA.setRole(Profile.Role.ADMIN);
        userAdministratorA.setPrivilege(Users.Privilege.ADMIN_MEDIUM);
        userAdministratorA.setParent(parent);
        testEntityManager.persist(userAdministratorA);
        testEntityManager.persist(administratorA);

        Administrator administratorB = new Administrator();
        Users userAdministratorB = new Users();
        userAdministratorB.setLogin("adminLoginB");
        userAdministratorB.setPassword("customerPassword");
        userAdministratorB.setFirstName("Balas");
        userAdministratorB.setLastName("Klas");
        userAdministratorB.setCreated(LocalDate.now());
        userAdministratorB.setEmail("Klas@gmail.com");
        userAdministratorB.setPhoneNumber("+225XXXXXXXX");
        userAdministratorB.setGender(Users.Gender.MAL);
        userAdministratorB.setRole(Profile.Role.ADMIN);
        userAdministratorB.setPrivilege(Users.Privilege.ADMIN_MEDIUM);
        userAdministratorB.setParent(parent);
        testEntityManager.persist(userAdministratorB);
        testEntityManager.persist(administratorB);

        List<Administrator> administrators = administratorService.find(root);
        assertFalse(administrators.isEmpty());
        assertEquals(2, administrators.size());
    }

    @Test
    public void findAdministrator() {
        Administrator administrator = new Administrator();
        Users userAdministrator = new Users();
        userAdministrator.setLogin("customerLogin");
        userAdministrator.setPassword("customerPassword");
        userAdministrator.setFirstName("Bolos");
        userAdministrator.setLastName("Dalas");
        userAdministrator.setCreated(LocalDate.now());
        userAdministrator.setEmail("Bolos@gmail.com");
        userAdministrator.setPhoneNumber("+225XXXXXXXX");
        userAdministrator.setGender(Users.Gender.MAL);
        userAdministrator.setRole(Profile.Role.CUSTOMER);
        userAdministrator.setPrivilege(Users.Privilege.CUSTOMER);
        userAdministrator = testEntityManager.persist(userAdministrator);
        administrator.setUser(userAdministrator);
        testEntityManager.persist(administrator);
        administrator = administratorService.find(userAdministrator);
        assertNotNull(administrator);
    }

    @Test
    public void findArtisans() {
        Artisan artisanA = new Artisan();
        Users userArtisanA = new Users();
        userArtisanA.setLogin("adminLoginA");
        userArtisanA.setPassword("customerPassword");
        userArtisanA.setFirstName("Bolos");
        userArtisanA.setLastName("Klos");
        userArtisanA.setCreated(LocalDate.now());
        userArtisanA.setEmail("Klos@gmail.com");
        userArtisanA.setPhoneNumber("+225XXXXXXXX");
        userArtisanA.setGender(Users.Gender.MAL);
        userArtisanA.setRole(Profile.Role.ARTISAN);
        userArtisanA.setPrivilege(Users.Privilege.ARTISAN);
        testEntityManager.persist(userArtisanA);
        testEntityManager.persist(artisanA);

        Artisan artisanB = new Artisan();
        Users userArtisanB = new Users();
        userArtisanB.setLogin("adminLoginB");
        userArtisanB.setPassword("customerPassword");
        userArtisanB.setFirstName("Balas");
        userArtisanB.setLastName("Klas");
        userArtisanB.setCreated(LocalDate.now());
        userArtisanB.setEmail("Klas@gmail.com");
        userArtisanB.setPhoneNumber("+225XXXXXXXX");
        userArtisanB.setGender(Users.Gender.MAL);
        userArtisanB.setRole(Profile.Role.ADMIN);
        userArtisanB.setPrivilege(Users.Privilege.ADMIN_MEDIUM);
        testEntityManager.persist(userArtisanB);
        testEntityManager.persist(artisanB);


        List<Artisan> artisans = artisanService.find();
        assertFalse(artisans.isEmpty());
        assertEquals(2, artisans.size());

    }

    @Test
    public void findArtisan() {
        Artisan artisanA = new Artisan();
        Users userArtisanA = new Users();
        userArtisanA.setLogin("adminLoginA");
        userArtisanA.setPassword("customerPassword");
        userArtisanA.setFirstName("Bolos");
        userArtisanA.setLastName("Klos");
        userArtisanA.setCreated(LocalDate.now());
        userArtisanA.setEmail("Klos@gmail.com");
        userArtisanA.setPhoneNumber("+225XXXXXXXX");
        userArtisanA.setGender(Users.Gender.MAL);
        userArtisanA.setRole(Profile.Role.ARTISAN);
        userArtisanA.setPrivilege(Users.Privilege.ARTISAN);
        long id = (long) testEntityManager.persistAndGetId(userArtisanA);

        Artisan artisan = artisanService.find(id);
        assertNotNull(artisan);
    }

    @Test
    public void deleteCustomer() {
        Customer customer = new Customer();
        Users userCustomer = new Users();
        userCustomer.setLogin("customerLogin");
        userCustomer.setPassword("customerPassword");
        userCustomer.setFirstName("Bolos");
        userCustomer.setLastName("Dalas");
        userCustomer.setCreated(LocalDate.now());
        userCustomer.setEmail("Bolos@gmail.com");
        userCustomer.setPhoneNumber("+225XXXXXXXX");
        userCustomer.setGender(Users.Gender.MAL);
        userCustomer.setRole(Profile.Role.CUSTOMER);
        userCustomer.setPrivilege(Users.Privilege.CUSTOMER);
        userCustomer = testEntityManager.persist(userCustomer);
        customer.setUser(userCustomer);
        long id = (long) testEntityManager.persistAndGetId(customer);
        boolean isDeleted = customerService.delete(id);
        assertTrue(isDeleted);
    }

    @Test
    public void deleteArtisan() {
        Artisan artisanA = new Artisan();
        Users userArtisanA = new Users();
        userArtisanA.setLogin("adminLoginA");
        userArtisanA.setPassword("customerPassword");
        userArtisanA.setFirstName("Bolos");
        userArtisanA.setLastName("Klos");
        userArtisanA.setCreated(LocalDate.now());
        userArtisanA.setEmail("Klos@gmail.com");
        userArtisanA.setPhoneNumber("+225XXXXXXXX");
        userArtisanA.setGender(Users.Gender.MAL);
        userArtisanA.setRole(Profile.Role.ARTISAN);
        userArtisanA.setPrivilege(Users.Privilege.ARTISAN);
        long id = (long) testEntityManager.persistAndGetId(userArtisanA);
        boolean isDeleted = artisanService.delete(id);
        assertTrue(isDeleted);
    }
}