package com.alloallo.backoffice.core.service;

import com.alloallo.backoffice.core.entity.*;
import com.alloallo.backoffice.core.model.Profile;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.stereotype.Service;


import javax.servlet.http.HttpSession;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Christian Amani on 13/12/2018.
 */
@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = {@ComponentScan.Filter(Service.class)})
public class UsersServiceTest {

    @MockBean
    private HttpSession httpSession;

    @MockBean
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UsersService usersService;
    @Autowired
    private ArtisanService artisanService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private TestEntityManager testEntityManager;

    private Users user;
    private Users userCustomer;
    private Users userArtisan;
    private Users parent;
    private Administrator administrator;
    private Category categoryA;

    @Before
    public void setup() {
        Mockito.when(httpSession.getAttribute("IP"))
                .thenReturn("IP : 220.168.123");
        Mockito.when(passwordEncoder.encode(Mockito.anyString()))
                .thenReturn("passwordEncoded");
        Mockito.when(passwordEncoder.matches(Mockito.anyString(), Mockito.anyString()))
                .thenReturn(true);

        parent = new Users();
        parent.setLogin("admin");
        parent.setPassword("password");
        parent.setFirstName("Elodie");
        parent.setLastName("Lue");
        parent.setCreated(LocalDate.now());
        parent.setEmail("elodie@gmail.com");
        parent.setPhoneNumber("+225XXXXXXXX");
        parent.setRole(Profile.Role.ADMIN);
        parent.setPrivilege(Users.Privilege.ADMIN_BIG);
        parent.setGender(Users.Gender.FEMALE);
        parent.setBirthDate(LocalDate.of(1988, 1, 1));
        parent = testEntityManager.persist(parent);

        administrator = new Administrator();
        administrator.setUser(parent);
        testEntityManager.persist(administrator);

        categoryA = new Category();
        categoryA.setLibelle("Categorie A");
        categoryA.setAdministrator(administrator);
        testEntityManager.persist(categoryA);

        user = new Users();
        user.setLogin("simple_admin");
        user.setPassword("password");
        user.setFirstName("Tashia");
        user.setLastName("Cleo");
        user.setCreated(LocalDate.now());
        user.setEmail("cloeTahshia@gmail.com");
        user.setPhoneNumber("+225XXXXXXXX");
        user.setRole(Profile.Role.ADMIN);
        user.setPrivilege(Users.Privilege.ADMIN_MEDIUM);
        user.setGender(Users.Gender.FEMALE);
        user.setParent(parent);

        userCustomer = new Users();
        userCustomer.setLogin("simple_user");
        userCustomer.setPassword("password");
        userCustomer.setFirstName("Doko");
        userCustomer.setLastName("Kami");
        userCustomer.setCreated(LocalDate.now());
        userCustomer.setEmail("Kami@gmail.com");
        userCustomer.setPhoneNumber("+225XXXXXXXX");
        userCustomer.setRole(Profile.Role.CUSTOMER);
        userCustomer.setPrivilege(Users.Privilege.CUSTOMER);
        userCustomer.setGender(Users.Gender.MAL);
        userCustomer.setParent(parent);

        userArtisan = new Users();
        userArtisan.setLogin("kouao");
        userArtisan.setPassword("password");
        userArtisan.setFirstName("Dani");
        userArtisan.setLastName("Olo");
        userArtisan.setCreated(LocalDate.now());
        userArtisan.setEmail("Olo@gmail.com");
        userArtisan.setPhoneNumber("+225XXXXXXXX");
        userArtisan.setGender(Users.Gender.MAL);
        userArtisan.setParent(parent);
        userArtisan.setRole(Profile.Role.ARTISAN);
        userArtisan.setPrivilege(Users.Privilege.ARTISAN);
    }

    @Test
    public void saveUser() {
        Users newUser = usersService.save(user, false);
        assertNotNull(newUser);
    }

    @Test
    public void updatedArtisanUser() {
        userArtisan = testEntityManager.persist(userArtisan);
        Artisan artisan = new Artisan();
        artisan.setUser(this.userArtisan);
        artisan.setAdministrator(administrator);
        artisan.setCategory(categoryA);
        artisan = testEntityManager.persist(artisan);
        final String newLastName = "Richard";
        this.userArtisan.setLastName(newLastName);
        this.userArtisan = usersService.save(this.userArtisan, false, artisan);
        assertEquals(newLastName, this.userArtisan.getLastName());
    }

    @Test
    public void updatedPasswordArtisanUser() {
        userArtisan = testEntityManager.persist(userArtisan);
        Artisan artisan = new Artisan();
        artisan.setUser(this.userArtisan);
        artisan.setAdministrator(administrator);
        artisan.setCategory(categoryA);
        artisan = testEntityManager.persist(artisan);
        this.userArtisan.setPassword("newPassword");
        this.userArtisan = usersService.save(this.userArtisan, true, artisan);
        assertNotNull(this.userArtisan);
        assertEquals("passwordEncoded", this.userArtisan.getPassword());
    }

    @Test
    public void saveArtisan() {
        Artisan artisan = new Artisan();
        artisan.setUser(this.userArtisan);
        artisan.setAdministrator(administrator);
        artisan.setCategory(categoryA);
        userArtisan = usersService.save(artisan);
        assertNotNull(userArtisan);
        List<Artisan> artisans = artisanService.find();
        assertFalse(artisans.isEmpty());
    }

    @Test
    public void updatedAdministratorUser() {
        Administrator administrator = new Administrator();
        administrator.setUser(parent);
        parent.setFirstName("Gbadie");
        Users updatedUser = usersService.save(parent, false, administrator);
        assertNotNull(updatedUser);
        assertEquals("Gbadie", updatedUser.getFirstName());
    }

    @Test
    public void updatePasswordAdministratorUser() {
        Administrator administrator = new Administrator();
        administrator.setUser(parent);
        parent.setPassword("newPassword");
        Users updatedUser = usersService.save(parent, true, administrator);
        assertNotNull(this.userArtisan);
        assertEquals("passwordEncoded", updatedUser.getPassword());
    }

    @Test
    public void saveAdministrator() {
        Administrator administrator = new Administrator();
        administrator.setUser(user);
        Users user = usersService.save(administrator);
        assertNotNull(user);
        administrator = administratorService.find(user);
        assertNotNull(administrator);
    }

    @Test
    public void updatedCustomerUser() {
        testEntityManager.persist(userCustomer);
        Customer customer = new Customer();
        customer.setUser(userCustomer);
        userCustomer.setFirstName("Bourou");
        Users updatedUser = usersService.save(userCustomer, false, customer);
        assertNotNull(updatedUser);
        assertEquals("Bourou", updatedUser.getFirstName());
    }

    @Test
    public void updatedPasswordCustomerUser() {
        testEntityManager.persist(userCustomer);
        Customer customer = new Customer();
        customer.setUser(userCustomer);
        userCustomer.setPassword("newPassword");
        Users updatedUser = usersService.save(userCustomer, true, customer);
        assertNotNull(updatedUser);
        assertEquals("passwordEncoded", updatedUser.getPassword());
    }

    @Test
    public void saveCustomer() {
        Customer customer = new Customer();
        customer.setUser(userCustomer);
        Users newUser = usersService.save(customer);
        assertNotNull(newUser);
        List<Customer> customers = customerService.find();
        assertFalse(customers.isEmpty());
    }

    @Test
    public void find() {
        testEntityManager.persist(user);
        testEntityManager.persist(userArtisan);
        testEntityManager.persist(userCustomer);
        List<Users> users = usersService.find();
        assertEquals(4, users.size());
    }

    @Test
    public void findUsers() {
        long id = (long) testEntityManager.persistAndGetId(user);
        Users user = usersService.find(id);
        assertNotNull(user);
        assertEquals(this.user.getLastName(), user.getLastName());
    }

    @Test
    public void findPassword() {
        long id = (long) testEntityManager.persistAndGetId(user);
        String password = usersService.findPassword(id);
        assertNotNull(password);
        assertEquals(user.getPassword(), password);
    }

    @Test
    public void delete() {
        user.setRole(Profile.Role.CUSTOMER);
        long id = (long) testEntityManager.persistAndGetId(user);
        boolean isDeleted = usersService.delete(id);
        assertTrue(isDeleted);
    }

    @Test
    public void deleteWithRole() {
        long id = (long) testEntityManager.persistAndGetId(userCustomer);
        Customer customer = new Customer();
        customer.setUser(userCustomer);
        boolean isDeleted = usersService.delete(id, Profile.Role.CUSTOMER);
        assertTrue(isDeleted);
    }

    @Test
    public void verifyPassword() {
        long id = (long) testEntityManager.persistAndGetId(userCustomer);
        boolean isMatched = usersService.verifyPassword(id, userCustomer.getPassword());
        assertTrue(isMatched);
    }

    @Test
    public void verifyLoginIfExist() {
        boolean isExist = usersService.verifyLoginIfExist(parent.getLogin());
        assertTrue(isExist);
    }
}