package com.alloallo.backoffice.core.repository;

import com.alloallo.backoffice.core.entity.Administrator;
import com.alloallo.backoffice.core.entity.Artisan;
import com.alloallo.backoffice.core.entity.Category;
import com.alloallo.backoffice.core.entity.Users;
import com.alloallo.backoffice.core.model.Profile;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Christian Amani on 13/12/2018.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class ArtisanRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private ArtisanRepository repository;

    private Users user1;
    private Users user2;
    private Administrator administrator;
    private Category category;

    @Before
    public void setup() {
        Users user = new Users();
        user.setLogin("simple_admin");
        user.setPassword("password");
        user.setFirstName("Tashia");
        user.setLastName("Cleo");
        user.setCreated(LocalDate.now());
        user.setEmail("cloeTahshia@gmail.com");
        user.setPhoneNumber("+225XXXXXXXX");
        user.setRole(Profile.Role.ADMIN);
        user.setPrivilege(Users.Privilege.ADMIN_MEDIUM);
        user.setGender(Users.Gender.FEMALE);
        testEntityManager.persist(user);

        administrator = new Administrator();
        administrator.setUser(user);
        testEntityManager.persist(administrator);

        category = new Category();
        category.setAdministrator(administrator);
        category.setLibelle("Categorie A");
        testEntityManager.persist(category);

        user1 = new Users();
        user1.setLogin("simple_user");
        user1.setPassword("password");
        user1.setFirstName("Doko");
        user1.setLastName("Kami");
        user1.setCreated(LocalDate.now());
        user1.setEmail("Kami@gmail.com");
        user1.setPhoneNumber("+225XXXXXXXX");
        user1.setRole(Profile.Role.ADMIN);
        user1.setPrivilege(Users.Privilege.ADMIN_SMALL);
        user1.setGender(Users.Gender.MAL);
        testEntityManager.persist(user1);

        user2 = new Users();
        user2.setLogin("simple_artisan");
        user2.setPassword("password");
        user2.setFirstName("armin");
        user2.setLastName("Kami");
        user2.setCreated(LocalDate.now());
        user2.setEmail("Kamiarmin@gmail.com");
        user2.setPhoneNumber("+225XXXXXXXX");
        user2.setRole(Profile.Role.ARTISAN);
        user2.setPrivilege(Users.Privilege.ARTISAN);
        user2.setGender(Users.Gender.MAL);
        testEntityManager.persist(user2);

    }

    @Test
    public void countByAdministrator() {
        Artisan artisan = new Artisan();
        artisan.setUser(user1);
        artisan.setCategory(category);
        artisan.setAdministrator(administrator);
        testEntityManager.persist(artisan);
        Artisan artisan2 = new Artisan();
        artisan2.setUser(user2);
        artisan2.setCategory(category);
        artisan2.setAdministrator(administrator);
        testEntityManager.persist(artisan2);
        long count = repository.countByAdministrator(administrator);
        assertEquals(2,count);
    }

    @Test
    public void deleteByUser_Id() {
        Artisan artisan = new Artisan();
        artisan.setUser(user1);
        artisan.setCategory(category);
        artisan.setAdministrator(administrator);
        testEntityManager.persist(artisan);
        repository.deleteByUser_Id(user1.getId());
        List<Artisan> all = repository.findAll();
        assertTrue(all.isEmpty());
    }
}