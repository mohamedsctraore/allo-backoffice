package com.alloallo.backoffice.core.repository;

import com.alloallo.backoffice.core.entity.Administrator;
import com.alloallo.backoffice.core.entity.Customer;
import com.alloallo.backoffice.core.entity.Users;
import com.alloallo.backoffice.core.model.Profile;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Christian Amani on 13/12/2018.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class CustomerRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private CustomerRepository repository;


    private Users user;
    private Administrator administrator;

    @Before
    public void setup() {
        user = new Users();
        user.setLogin("simple_admin");
        user.setPassword("password");
        user.setFirstName("Tashia");
        user.setLastName("Cleo");
        user.setCreated(LocalDate.now());
        user.setEmail("cloeTahshia@gmail.com");
        user.setPhoneNumber("+225XXXXXXXX");
        user.setRole(Profile.Role.ADMIN);
        user.setPrivilege(Users.Privilege.ADMIN_MEDIUM);
        user.setGender(Users.Gender.FEMALE);
        testEntityManager.persist(user);

        administrator = new Administrator();
        administrator.setUser(user);
        testEntityManager.persist(administrator);
    }


    @Test
    public void deleteByUser_Id() {
        Customer customer = new Customer();
        customer.setUser(user);
        testEntityManager.persist(customer);
        repository.deleteByUser_Id(user.getId());
        List<Customer> all = repository.findAll();
        assertTrue(all.isEmpty());
    }
}