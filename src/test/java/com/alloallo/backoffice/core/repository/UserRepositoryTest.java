package com.alloallo.backoffice.core.repository;

import com.alloallo.backoffice.core.entity.Users;
import com.alloallo.backoffice.core.model.Profile;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

import static org.junit.Assert.*;

/**
 * @author Christian Amani on 13/12/2018.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private UserRepository repository;

    @Before
    public void setup() {
        Users user1 = new Users();
        user1.setLogin("simple_user");
        user1.setPassword("password");
        user1.setFirstName("Doko");
        user1.setLastName("Kami");
        user1.setCreated(LocalDate.now());
        user1.setEmail("Kami@gmail.com");
        user1.setPhoneNumber("+225XXXXXXXX");
        user1.setRole(Profile.Role.ADMIN);
        user1.setPrivilege(Users.Privilege.ADMIN_SMALL);
        user1.setGender(Users.Gender.MAL);
        testEntityManager.persist(user1);

        Users user2 = new Users();
        user2.setLogin("simple_artisan");
        user2.setPassword("password");
        user2.setFirstName("armin");
        user2.setLastName("Kami");
        user2.setCreated(LocalDate.now());
        user2.setEmail("Kamiarmin@gmail.com");
        user2.setPhoneNumber("+225XXXXXXXX");
        user2.setRole(Profile.Role.ARTISAN);
        user2.setPrivilege(Users.Privilege.ARTISAN);
        user2.setGender(Users.Gender.MAL);
        testEntityManager.persist(user2);
    }

    @Test
    public void findByLogin() {
        Users artisan = repository.findByLogin("simple_artisan");
        assertNotNull(artisan);
    }

    @Test
    public void existsByLogin() {
        boolean isExist = repository.existsByLogin("simple_user");
        assertTrue(isExist);
    }
}