package com.alloallo.backoffice.core.repository;

import com.alloallo.backoffice.core.entity.Administrator;
import com.alloallo.backoffice.core.entity.Users;
import com.alloallo.backoffice.core.model.Profile;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Christian Amani on 13/12/2018.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class AdministratorRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private AdministratorRepository repository;

    private Users user;
    private Users user2;
    private Users parent;


    @Before
    public void setup() {
        parent = new Users();
        parent.setLogin("admin");
        parent.setPassword("password");
        parent.setFirstName("Elodie");
        parent.setLastName("Lue");
        parent.setCreated(LocalDate.now());
        parent.setEmail("elodie@gmail.com");
        parent.setPhoneNumber("+225XXXXXXXX");
        parent.setRole(Profile.Role.ADMIN);
        parent.setPrivilege(Users.Privilege.ADMIN_BIG);
        parent.setGender(Users.Gender.FEMALE);
        parent.setBirthDate(LocalDate.of(1988, 1, 1));
        parent = testEntityManager.persist(parent);

        user = new Users();
        user.setLogin("simple_admin");
        user.setPassword("password");
        user.setFirstName("Tashia");
        user.setLastName("Cleo");
        user.setCreated(LocalDate.now());
        user.setEmail("cloeTahshia@gmail.com");
        user.setPhoneNumber("+225XXXXXXXX");
        user.setRole(Profile.Role.ADMIN);
        user.setPrivilege(Users.Privilege.ADMIN_MEDIUM);
        user.setGender(Users.Gender.FEMALE);
        user.setParent(parent);

        user2 = new Users();
        user2.setLogin("simple_user");
        user2.setPassword("password");
        user2.setFirstName("Doko");
        user2.setLastName("Kami");
        user2.setCreated(LocalDate.now());
        user2.setEmail("Kami@gmail.com");
        user2.setPhoneNumber("+225XXXXXXXX");
        user2.setRole(Profile.Role.ADMIN);
        user2.setPrivilege(Users.Privilege.ADMIN_SMALL);
        user2.setGender(Users.Gender.MAL);
        user2.setParent(parent);
    }

    @Test
    public void findByUser() {
        user = testEntityManager.persist(user);
        Administrator administrator = new Administrator();
        administrator.setUser(user);
        testEntityManager.persist(administrator);
        administrator = repository.findByUser(user);
        assertNotNull(administrator);
        String login = administrator.getUser()
                .getLogin();
        assertEquals("simple_admin", login);
    }

    @Test
    public void findByUser_Parent() {
        user = testEntityManager.persist(user);
        Administrator administrator = new Administrator();
        administrator.setUser(user);
        testEntityManager.persist(administrator);

        List<Administrator> users = repository.findByUser_Parent(parent);
        assertFalse(users.isEmpty());
        Users adminUser = users.get(0)
                .getUser();
        assertEquals(adminUser,user);
    }

    @Test
    public void countByUser_Parent() {
        user = testEntityManager.persist(user);
        user2 = testEntityManager.persist(user2);
        Administrator administrator1 = new Administrator();
        administrator1.setUser(user);
        testEntityManager.persist(administrator1);
        Administrator administrator2 = new Administrator();
        administrator2.setUser(user2);
        testEntityManager.persist(administrator2);
        long count = repository.countByUser_Parent(parent);
        assertEquals(2,count);
    }

    @Test
    public void deleteByUser_Id() {
        user = testEntityManager.persist(user);
        Administrator administrator = new Administrator();
        administrator.setUser(user);
        testEntityManager.persist(administrator);
        repository.deleteByUser_Id(user.getId());
        assertNull(repository.findByUser(user));
    }
}