package com.alloallo.backoffice.core.repository;

import com.alloallo.backoffice.core.entity.Administrator;
import com.alloallo.backoffice.core.entity.Category;
import com.alloallo.backoffice.core.entity.Users;
import com.alloallo.backoffice.core.model.Profile;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

import static org.junit.Assert.*;

/**
 * @author Christian Amani on 13/12/2018.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class CategoryRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private CategoryRepository repository;

    private Administrator administrator;

    @Before
    public void setup() {
        Users user = new Users();
        user.setLogin("simple_admin");
        user.setPassword("password");
        user.setFirstName("Tashia");
        user.setLastName("Cleo");
        user.setCreated(LocalDate.now());
        user.setEmail("cloeTahshia@gmail.com");
        user.setPhoneNumber("+225XXXXXXXX");
        user.setRole(Profile.Role.ADMIN);
        user.setPrivilege(Users.Privilege.ADMIN_MEDIUM);
        user.setGender(Users.Gender.FEMALE);
        testEntityManager.persist(user);

        administrator = new Administrator();
        administrator.setUser(user);
        testEntityManager.persist(administrator);
    }

    @Test
    public void countByAdministrator() {
        Category categoryA = new Category();
        categoryA.setLibelle("Categorie A");
        categoryA.setAdministrator(administrator);
        testEntityManager.persist(categoryA);
        Category categoryB = new Category();
        categoryB.setLibelle("Categorie B");
        categoryB.setAdministrator(administrator);
        testEntityManager.persist(categoryB);
        long count = repository.countByAdministrator(administrator);
        assertEquals(2,count);
    }
}